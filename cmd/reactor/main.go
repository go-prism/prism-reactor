package main

import (
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-api/pkg/httputils"
	"gitlab.com/go-prism/prism-api/pkg/jobs"
	"gitlab.com/go-prism/prism-api/pkg/logging"
	"gitlab.com/go-prism/prism-api/pkg/metrics"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/pkg/rpclient"
	"gitlab.com/go-prism/prism-rpc/pkg/rpcserv"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	api2 "gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/api"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache/partition"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	jobs2 "gitlab.dcas.dev/prism/prism-reactor/pkg/jobs"
	reactor2 "gitlab.dcas.dev/prism/prism-reactor/pkg/reactor"
	"google.golang.org/grpc"
	"os"
	"os/signal"
	"syscall"
)

type environment struct {
	S3        cache.S3Config
	Port      int `default:"8081"`
	Log       logging.Config
	ServerTLS httputils.TLSConfig `split_words:"true"`
	ClientTLS httputils.TLSConfig `split_words:"true"`

	ServiceURL string `split_words:"true" default:"localhost:8080"`
	MasterKey  string `split_words:"true"`
}

func main() {
	// read config
	var e environment
	err := envconfig.Process("prism", &e)
	if err != nil {
		log.WithError(err).Fatal("failed to read environment")
		return
	}

	// configure logging
	logging.Init(&e.Log)

	// setup clients
	sc, err := rpclient.NewAutoConn(e.ServiceURL, e.ClientTLS.CertFile, e.ClientTLS.KeyFile, e.ClientTLS.CaFile)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to service")
		return
	}
	// setup services
	s3, err := cache.NewS3v2(e.S3)
	if err != nil {
		log.WithError(err).Fatal("failed to create s3 session")
		return
	}
	// check that a master key is set
	if e.MasterKey == "" {
		log.Fatal("PRISM_MASTER_KEY must be set to a secure value.")
		return
	}
	rc := v1.NewReactorClient(sc)
	h := hasher.NewScrypt(e.MasterKey, reactor2.DefaultRestrictedHeaders)
	rodPool, err := reactor2.NewRodPool(
		rc,
		s3,
		v1.NewCacheClient(sc),
		api2.NewRemotesClient(sc),
		h,
		[]partition.Mutator{partition.NewGitLabMutator()},
	)
	if err != nil {
		log.Fatal("failed to setup rodpool")
		return
	}
	reactorSrv := api.NewAPI(rodPool, s3)

	// start the event loop
	runner := &jobs.Runner{}
	runner.Init([]jobs.BaseRunner{
		jobs2.NewStatusJob(rc, rodPool),
		jobs2.NewConfigJob(rc, rodPool),
	}, nil)

	// setup routes
	router := mux.NewRouter()
	// enable Prometheus metrics
	metrics.Init(router, "/metrics")
	router.HandleFunc("/ping", httputils.PingFunc)

	// start the server
	gsrv := grpc.NewServer(utils.GetOpts()...)
	reactor.RegisterReactorServer(gsrv, reactorSrv)

	srv, err := rpcserv.NewDualPurpose(e.Port, gsrv, router)
	if err != nil {
		log.WithError(err).Fatal("failed to open listener")
		return
	}
	// start http server
	go func() {
		log.Fatal(srv.ListenAndServeAuto(e.ServerTLS.CertFile, e.ServerTLS.KeyFile, e.ServerTLS.CaFile))
	}()

	// wait for a signal
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGTERM, syscall.SIGINT)
	sig := <-sigC
	log.Infof("received SIGTERM/SIGINT (%s), shutting down...", sig)
}
