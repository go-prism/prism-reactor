package jobs

import (
	"context"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/reactor"
	"os"
	"time"
)

type StatusJob struct {
	reactorClient v1.ReactorClient
	pool          *reactor.RodPool
}

// NewStatusJob creates a new instance of StatusJob
func NewStatusJob(rc v1.ReactorClient, pool *reactor.RodPool) *StatusJob {
	j := new(StatusJob)
	j.reactorClient = rc
	j.pool = pool

	return j
}

func (*StatusJob) GetInterval() uint64 {
	return 30
}

func (j *StatusJob) Do() {
	ctx := context.TODO()
	log.WithContext(ctx).Info("collecting statistics for this reactor")
	name, err := os.Hostname()
	if err != nil {
		log.WithError(err).Error("failed to read hostname of this Reactor")
		return
	}
	if _, err := j.reactorClient.CreateStatus(context.TODO(), &v1.ReactorStatus{
		Id:          uuid.New().String(),
		ConfigHash:  "",
		Time:        uint64(time.Now().Unix()),
		Name:        name,
		ControlRods: j.pool.Keys(),
	}); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to submit reactor statistics")
	}
}
