package jobs

import (
	"gitlab.com/go-prism/prism-api/pkg/jobs"
)

// interface guard
var _ jobs.BaseRunner = &StatusJob{}
