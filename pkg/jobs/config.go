package jobs

import (
	"context"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/reactor"
	"google.golang.org/protobuf/types/known/emptypb"
)

// ConfigJob checks whether the reactors
// config is up-to-date.
type ConfigJob struct {
	reactorClient v1.ReactorClient
	pool          *reactor.RodPool
	hash          string
}

// NewConfigJob creates a new instance of ConfigJob
func NewConfigJob(rc v1.ReactorClient, pool *reactor.RodPool) *ConfigJob {
	j := new(ConfigJob)
	j.reactorClient = rc
	j.pool = pool

	return j
}

func (*ConfigJob) GetInterval() uint64 {
	return 10
}

func (j *ConfigJob) Do() {
	ctx := context.TODO()
	config, err := j.reactorClient.GetConfig(ctx, &emptypb.Empty{})
	if err != nil {
		log.WithError(err).Error("failed to fetch config from API")
		return
	}
	if config.GetHash() == j.hash {
		log.Debugf("config hash '%s' is up-to-date", j.hash)
		return
	}
	log.Infof("detected mismatch in config hash (expected: '%s', got: '%s')", config.GetHash(), j.hash)
	// if we have a configuration mismatch, purge the reactor
	j.pool.Purge(ctx)
	j.hash = config.GetHash()
}
