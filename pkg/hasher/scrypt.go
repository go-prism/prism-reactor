package hasher

import (
	"encoding/hex"
	"fmt"
	"github.com/djcass44/go-utils/pkg/sliceutils"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"golang.org/x/crypto/scrypt"
	"sort"
	"strings"
)

type Scrypt struct {
	masterKey                string
	defaultRestrictedHeaders []string
}

func NewScrypt(masterKey string, defaultRestrictedHeaders []string) *Scrypt {
	s := new(Scrypt)
	s.masterKey = masterKey
	s.defaultRestrictedHeaders = defaultRestrictedHeaders

	return s
}

// GetHash get the sha256 hash of the restricted headers
// with a RequestHeaders map
func (s *Scrypt) GetHash(remote *v1.Remote, headers map[string]string) (string, error) {
	hashable := s.toHashable(remote, headers)
	// hash the concatenated string
	hash, err := scrypt.Key([]byte(hashable), []byte(s.masterKey), 16384, 8, 1, 32)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(hash), nil
}

// toHashable converts the restricted headers to a
// consistent string format
func (s *Scrypt) toHashable(remote *v1.Remote, headers map[string]string) string {
	restricted := append(remote.RestrictedHeaders, s.defaultRestrictedHeaders...)
	var h []string
	for k, v := range headers {
		if sliceutils.Includes(restricted, k) {
			h = append(h, fmt.Sprintf("%s=%s", k, v))
		}
	}
	sort.Strings(h)
	return strings.Join(h, ",")
}
