package hasher

import v1 "gitlab.com/go-prism/prism-rpc/domain/v1"

type Hasher interface {
	GetHash(remote *v1.Remote, headers map[string]string) (string, error)
}
