package hasher_test

import (
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/reactor"
	"testing"
)

// interface guard
var _ hasher.Hasher = &hasher.Scrypt{}

var remote = &v1.Remote{
	RestrictedHeaders: []string{"XYZ"},
}

var getHashTests = []struct {
	name string
	in   reactor.RequestHeaders
	out  string
}{
	{
		"Authorization header is hashed",
		map[string]string{"Authorization": "foo"},
		"add90044fc4e594b4916c795986d221551d18720f3c1351bcb022c8199745d0d",
	},
	{
		"Standard headers are ignored",
		map[string]string{"Authorization": "foo", "Foo": "bar"},
		"add90044fc4e594b4916c795986d221551d18720f3c1351bcb022c8199745d0d",
	},
	{
		"Remote headers are hashed",
		map[string]string{"Authorization": "foo", "Foo": "bar", "XYZ": "arst"},
		"0fb37209d267485791688a37791faed21953ccb208a3c043ed2b15e0fc30fa27",
	},
}

func TestScrypt_GetHash(t *testing.T) {
	var h = hasher.NewScrypt("hunter2", reactor.DefaultRestrictedHeaders)
	for _, tt := range getHashTests {
		t.Run(tt.name, func(t *testing.T) {
			str, err := h.GetHash(remote, tt.in)
			assert.NoError(t, err)
			assert.Equal(t, tt.out, str)
		})
	}
}

func TestScript_differentKeys(t *testing.T) {
	var h1 = hasher.NewScrypt("hunter2", reactor.DefaultRestrictedHeaders)
	var h2 = hasher.NewScrypt("password", reactor.DefaultRestrictedHeaders)
	k1, err := h1.GetHash(remote, map[string]string{})
	assert.NoError(t, err)
	k2, err := h2.GetHash(remote, map[string]string{})
	assert.NoError(t, err)
	assert.NotEqualValues(t, k1, k2)
}

// BenchmarkRequestHeaders_GetHash executes
// the above tests repeatedly so that we know
// the result doesn't change between executions.
func BenchmarkScrypt_GetHash(b *testing.B) {
	var h = hasher.NewScrypt("hunter2", reactor.DefaultRestrictedHeaders)
	for i := 0; i < b.N; i++ {
		for _, tt := range getHashTests {
			b.Run(tt.name, func(b *testing.B) {
				str, err := h.GetHash(remote, tt.in)
				assert.NoError(b, err)
				assert.Equal(b, tt.out, str)
			})
		}
	}
}
