package api

import (
	"bytes"
	"context"
	"errors"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/golang/protobuf/ptypes/empty"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache"
	reactor2 "gitlab.dcas.dev/prism/prism-reactor/pkg/reactor"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"os"
	"strings"
)

var (
	// ErrMissingRequest is thrown when a stream was given but the describing request
	// object was never sent.
	ErrMissingRequest = errors.New("no request was given")
)

type API struct {
	reactor.UnimplementedReactorServer
	pool     *reactor2.RodPool
	provider cache.Provider
}

func NewAPI(pool *reactor2.RodPool, p cache.Provider) *API {
	api := new(API)
	api.pool = pool
	api.provider = p

	return api
}

func (api *API) GetStatus(ctx context.Context, _ *empty.Empty) (*reactor.ReactorStatus, error) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{"id": id}
	log.WithContext(ctx).WithFields(fields).Info("collecting statistics for this reactor")
	name, err := os.Hostname()
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to read hostname of this Reactor")
	}
	return &reactor.ReactorStatus{
		Id:          name,
		ControlRods: api.pool.Keys(),
	}, nil
}

func (api *API) InvalidateControlRod(ctx context.Context, req *reactor.InvalidateControlRodRequest) (*empty.Empty, error) {
	api.pool.Delete(ctx, req.Refraction)
	return &empty.Empty{}, nil
}

// GetObject
//
// Deprecated: use GetObjectV2 instead
func (api *API) GetObject(context.Context, *reactor.GetObjectRequest) (*reactor.GetObjectResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetObject not implemented")
}

func (api *API) GetObjectV2(req *reactor.GetObjectRequest, stream reactor.Reactor_GetObjectV2Server) error {
	fields := log.Fields{"path": req.GetPath(), "ref": req.GetRefraction(), "method": req.GetMethod()}
	log.WithContext(stream.Context()).WithFields(fields).Info("accepting request to fetch object")
	cr, err := api.pool.Get(stream.Context(), req.GetRefraction())
	if err != nil {
		return err
	}
	return cr.Do(stream.Context(), req, stream)
}

func (api *API) HasObject(ctx context.Context, req *reactor.GetObjectRequest) (*reactor.HasObjectResponse, error) {
	fields := log.Fields{"path": req.GetPath()}
	log.WithContext(ctx).WithFields(fields).Info("accepting request to lookup object")
	cr, err := api.pool.Get(ctx, req.GetRefraction())
	if err != nil {
		return nil, err
	}
	ok, err := cr.HasObject(ctx, req)
	if err != nil {
		return nil, err
	}
	log.WithContext(ctx).WithFields(fields).Infof("found file: %v", ok)
	return &reactor.HasObjectResponse{
		Ok: ok,
	}, nil
}

func (api *API) PutObjectV2(stream reactor.Reactor_PutObjectV2Server) error {
	log.WithContext(stream.Context()).Info("accepting request to upload object")
	var request *reactor.GetObjectRequest
	var buf = &bytes.Buffer{}
	// read the stream until it stops giving us data
	for {
		resp, err := stream.Recv()
		if err != nil {
			// bail out on an EOF
			if errors.Is(err, io.EOF) {
				log.WithContext(stream.Context()).Info("received EOF, exiting reader...")
				break
			}
			log.WithError(err).WithContext(stream.Context()).Error("got unexpected error reading PutObjectV2")
			return err
		}
		// figure out which type of message we've received
		switch v := resp.GetMsg().(type) {
		case *reactor.PutObjectV2Request_Request:
			request = v.Request
		case *reactor.PutObjectV2Request_Content:
			buf.Write(v.Content)
		}
	}
	log.WithContext(stream.Context()).Infof("received %d bytes from client", buf.Len())
	// we must receive a request object at some point
	if request == nil {
		log.WithError(ErrMissingRequest).WithContext(stream.Context()).Error("invalid usage, request MUST be delivered by the client")
		return ErrMissingRequest
	}
	// fetch a matching controlRod
	cr, err := api.pool.Get(stream.Context(), request.GetRefraction())
	if err != nil {
		return err
	}
	// upload data
	if err = cr.PutObject(stream.Context(), request, buf); err != nil {
		return err
	}
	// close the stream
	return stream.SendAndClose(&empty.Empty{})
}

func (api *API) GetRefractInfo(ctx context.Context, req *reactor.GetRefractInfoRequest) (*reactor.GetRefractInfoResponse, error) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id":    id,
		"objID": req.Refraction,
	}
	log.WithContext(ctx).WithFields(fields).Info("collecting refraction info")
	info, err := api.provider.GetSize(ctx, strings.ToLower(req.Refraction))
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (api *API) ListObject(ctx context.Context, req *reactor.ListObjectRequest) (*reactor.ListObjectResponse, error) {
	fields := log.Fields{
		"path": req.GetPrefix(),
		"ref":  req.GetRefraction(),
	}
	log.WithContext(ctx).WithFields(fields).Info("accepting request to list objects")
	cr, err := api.pool.Get(ctx, req.GetRefraction())
	if err != nil {
		return nil, err
	}
	items, err := cr.ListObjects(ctx, req.GetPrefix())
	if err != nil {
		return nil, err
	}
	return &reactor.ListObjectResponse{
		Objects: items,
	}, nil
}
