package api_test

import (
	"bytes"
	"context"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	reactor2 "gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/internal/testutils"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/api"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/reactor"
)

var h = hasher.NewScrypt("password", reactor.DefaultRestrictedHeaders)

func TestNewAPI(t *testing.T) {
	svc := api.NewAPI(nil, nil)
	assert.NotNil(t, svc)
}

func TestAPI_HasObject(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	defer ts.Close()

	cache := testutils.NewTestProvider()
	testRodPool, err := reactor.NewRodPool(
		&testutils.TestReactorClient{
			Srv: ts,
		},
		cache,
		nil,
		&testutils.TestRemotesClient{},
		h,
		nil,
	)
	assert.NoError(t, err)
	svc := api.NewAPI(testRodPool, cache)
	resp, err := svc.HasObject(context.TODO(), &reactor2.GetObjectRequest{
		Refraction: "test",
		Path:       "test://test.txt",
		Method:     http.MethodGet,
		Headers:    nil,
	})
	assert.NoError(t, err)
	assert.True(t, resp.Ok)

	resp, err = svc.HasObject(context.TODO(), &reactor2.GetObjectRequest{
		Refraction: "test",
		Path:       "/test.txt",
		Method:     http.MethodGet,
		Headers:    nil,
	})
	assert.NoError(t, err)
	assert.False(t, resp.Ok)
}

func TestAPI_GetObject(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	defer ts.Close()

	testRodPool, err := reactor.NewRodPool(
		&testutils.TestReactorClient{
			Srv: ts,
		},
		testutils.NewTestProvider(),
		&testutils.TestClient{},
		&testutils.TestRemotesClient{},
		h,
		nil,
	)
	assert.NoError(t, err)
	svc := api.NewAPI(testRodPool, nil)
	err = svc.GetObjectV2(&reactor2.GetObjectRequest{
		Refraction: "test",
		Path:       "/test.txt",
		Method:     http.MethodGet,
		Headers:    nil,
	}, &testutils.TestReactorStream{
		T:               t,
		ExpectedCode:    http.StatusOK,
		ExpectedContent: []byte("OK"),
	})
	assert.NoError(t, err)
}

func TestAPI_GetObject_BadRefraction(t *testing.T) {
	testRodPool, err := reactor.NewRodPool(
		&testutils.TestReactorClient{
			Srv: httptest.NewServer(http.NotFoundHandler()),
		},
		testutils.NewTestProvider(),
		&testutils.TestClient{},
		&testutils.TestRemotesClient{},
		h,
		nil,
	)
	assert.NoError(t, err)
	svc := api.NewAPI(testRodPool, nil)
	err = svc.GetObjectV2(&reactor2.GetObjectRequest{
		Refraction: "",
		Path:       "",
		Method:     http.MethodGet,
		Headers:    nil,
	}, &testutils.TestReactorStream{
		T:            t,
		ExpectedCode: 0,
	})
	assert.EqualError(t, err, "you asked for this")
}

func TestAPI_GetRefractInfo(t *testing.T) {
	tp := testutils.NewTestProvider()
	assert.NoError(t, tp.PutObject(context.TODO(), "test/test.txt", ioutil.NopCloser(bytes.NewBufferString("finna data"))))
	svc := api.NewAPI(nil, tp)
	resp, err := svc.GetRefractInfo(context.TODO(), &reactor2.GetRefractInfoRequest{
		Refraction: "test",
	})
	assert.NoError(t, err)
	assert.EqualValues(t, 1, resp.Files)
}

func TestAPI_InvalidateControlRod(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	defer ts.Close()

	testRodPool, err := reactor.NewRodPool(
		&testutils.TestReactorClient{
			Srv: ts,
		},
		testutils.NewTestProvider(),
		&testutils.TestClient{},
		&testutils.TestRemotesClient{},
		h,
		nil,
	)
	assert.NoError(t, err)
	_, err = testRodPool.Get(context.TODO(), "test")
	assert.ElementsMatch(t, testRodPool.Keys(), []string{"test"})
	assert.NoError(t, err)
	svc := api.NewAPI(testRodPool, nil)
	_, err = svc.InvalidateControlRod(context.TODO(), &reactor2.InvalidateControlRodRequest{
		Refraction: "test",
	})
	assert.NoError(t, err)
	assert.Empty(t, testRodPool.Keys())
}

func TestAPI_GetStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	defer ts.Close()

	testRodPool, err := reactor.NewRodPool(
		&testutils.TestReactorClient{
			Srv: ts,
		},
		testutils.NewTestProvider(),
		&testutils.TestClient{},
		&testutils.TestRemotesClient{},
		h,
		nil,
	)
	assert.NoError(t, err)
	_, err = testRodPool.Get(context.TODO(), "test")
	assert.ElementsMatch(t, testRodPool.Keys(), []string{"test"})
	assert.NoError(t, err)
	svc := api.NewAPI(testRodPool, nil)

	status, err := svc.GetStatus(context.TODO(), nil)
	assert.NoError(t, err)
	assert.ElementsMatch(t, status.ControlRods, testRodPool.Keys())
}
