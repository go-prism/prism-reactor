package reactor

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	daov1 "gitlab.com/go-prism/prism-api/pkg/dto/v1"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache/partition"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/policy"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

type Neutron struct {
	base     *BaseControlRod
	fetcher  *Fetcher
	provider cache.Provider
	hasher   hasher.Hasher
}

// NewNeutron creates an instance of Neutron
func NewNeutron(remotes []*v1.Remote, p cache.Provider, cacheClient v1.CacheClient, hasher hasher.Hasher, mutators []partition.Mutator) (*Neutron, error) {
	cr := new(Neutron)
	cr.base = &BaseControlRod{
		hasher:      hasher,
		cacheClient: cacheClient,
		provider:    p,
		mutators:    mutators,
	}
	cr.provider = p
	cr.hasher = hasher
	// setup the fetcher
	fetcher, err := NewFetcher(remotes)
	if err != nil {
		return nil, err
	}
	cr.fetcher = fetcher
	cr.base.fetcher = fetcher
	return cr, nil
}

// doCache handles cache interaction before querying the remote.
//
// 1. check if the cache is holding the file
// 2. stream the file back to the client if we found it
//
// this function must return an error to indicate that the file was not retrieved
// from the cache.
func (n *Neutron) doCache(ctx context.Context, policy policy.RemotePolicy, req *reactor.GetObjectRequest, resp reactor.Reactor_GetObjectV2Server) error {
	remote := policy.GetRemote()
	fields := log.Fields{
		"path":   req.Path,
		"remote": remote.GetName(),
	}
	log.WithContext(ctx).WithFields(fields).Info("checking cache for existing object")
	headers := n.fetcher.stripHeaders(ctx, remote, req.GetHeaders())
	for _, m := range n.base.mutators {
		headers = m.Apply(ctx, remote, headers)
	}
	// get the header-hash so that we can check the
	// appropriate cache partition
	hash, err := n.hasher.GetHash(remote, headers)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
		return err
	}
	storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(remote.GetName()), req.Path, hash)
	if ok, err := n.provider.HasObject(ctx, storagePath); err != nil || !ok {
		log.WithError(err).WithContext(ctx).WithFields(fields).Errorf("failed to find existing object in remote: '%s'", remote.GetName())
		return err
	}
	log.WithContext(ctx).WithFields(fields).Infof("found cache entry in remote: '%s'", remote.GetName())
	// confirm that the file is not 0 bytes.
	// sometimes failed downloads cache an empty file
	// which causes issues for client such as Maven
	if info, err := n.provider.GetSize(ctx, storagePath); err != nil {
		log.WithError(err).WithContext(ctx).WithFields(log.Fields{
			"remote": remote.GetName(),
			"path":   storagePath,
		}).Error("failed to read for cached object")
		return err
	} else if info.Size <= 0 {
		log.WithContext(ctx).WithFields(log.Fields{
			"remote": remote.GetName(),
			"path":   storagePath,
		}).Warning("detected cached file with size of 0")
		metricCacheCorruptGet.Inc()
		return ErrCacheMissed // todo change this to a proper error
	}
	n.base.createCacheEntry(ctx, remote.GetId(), remote.GetId(), req.Path, hash)
	// download the file
	data, err := n.provider.StreamGetObject(ctx, storagePath)
	if err != nil {
		return err
	}
	// stream the file back over the rpc connection
	//
	// 2021/06/23 - sending a 304 causes an error since
	// you're not allowed to send a body with it :(
	return n.base.streamResponse(ctx, data, http.StatusOK, resp)
}

// Do executes a proxied request
func (n *Neutron) Do(ctx context.Context, req *reactor.GetObjectRequest, resp reactor.Reactor_GetObjectV2Server) error {
	fields := log.Fields{
		"path":   req.Path,
		"method": req.Method,
		"remote": req.GetRefraction(),
	}
	log.WithContext(ctx).WithFields(fields).Info("starting neutron control rod")
	pol, ok := n.fetcher.getRemoteByName(req.GetRefraction())
	if !ok {
		log.WithContext(ctx).WithFields(fields).Warning("failed to find remote")
		return ErrMissingRemote
	}
	var remote = pol.GetRemote()
	// check the cache first
	err := n.doCache(ctx, pol, req, resp)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("unable to find cache entry, we will insert the control rod")
	} else {
		return nil
	}
	if remote.GetArchetype() == daov1.ArchetypeGo {
		log.WithContext(ctx).WithFields(fields).Warning("golang controlrod was unable to find cached data, we will return here so the plugin can handle it")
		return resp.Send(&reactor.GetObjectV2Response{
			Content: nil,
			Code:    http.StatusNotFound,
		})
	}
	log.WithContext(ctx).WithFields(fields).Info("inserting control rod")
	defer func() {
		log.WithContext(ctx).WithFields(fields).Info("removing control rod")
	}()
	return n.base.fetch(ctx, remote, remote.GetName(), remote.GetId(), req, resp, func() *RetrieveObjectResult {
		return n.fetcher.GetByName(ctx, req, req.GetRefraction())
	})
}

func (n *Neutron) ListObjects(ctx context.Context, prefix string) ([]string, error) {
	log.WithContext(ctx).Infof("listing objects under path: '%s'", prefix)
	return n.provider.ListObjects(ctx, prefix)
}

func (n *Neutron) HasObject(ctx context.Context, req *reactor.GetObjectRequest) (bool, error) {
	hash, err := n.hasher.GetHash(&v1.Remote{RestrictedHeaders: DefaultRestrictedHeaders}, req.Headers)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
		return false, err
	}
	storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(req.GetRefraction()), req.Path, hash)
	log.WithContext(ctx).Infof("check for existance of object at path: '%s'", storagePath)
	return n.provider.HasObject(ctx, storagePath)
}

func (n *Neutron) PutObject(ctx context.Context, req *reactor.GetObjectRequest, r io.Reader) error {
	hash, err := n.hasher.GetHash(&v1.Remote{RestrictedHeaders: DefaultRestrictedHeaders}, req.Headers)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
		return err
	}
	storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(req.GetRefraction()), req.Path, hash)
	log.WithContext(ctx).Infof("uploading object to path: '%s'", storagePath)
	// upload the object to the cache
	if err := n.provider.PutObject(ctx, storagePath, ioutil.NopCloser(r)); err != nil {
		return err
	}
	return nil
}
