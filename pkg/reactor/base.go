package reactor

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache/partition"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

type BaseControlRod struct {
	hasher      hasher.Hasher
	cacheClient v1.CacheClient
	provider    cache.Provider
	fetcher     *Fetcher
	mutators    []partition.Mutator
}

func (cr *BaseControlRod) fetch(ctx context.Context, remote *v1.Remote, refractName, refractID string, req *reactor.GetObjectRequest, resp reactor.Reactor_GetObjectV2Server, f func() *RetrieveObjectResult) error {
	// fetch the data
	res := f()
	// handle the error
	if res.Error != nil {
		// if we got a final request, return it
		if res.Response != nil {
			return cr.streamResponse(ctx, res.Response.Body, int32(res.Response.StatusCode), resp)
		}
		// otherwise send a generic failure message
		return resp.Send(&reactor.GetObjectV2Response{
			Content: nil,
			Code:    http.StatusBadRequest,
		})
	}

	// handle the data once we've got it back
	log.WithContext(ctx).Debugf("expecting to cache %d bytes", res.Response.ContentLength)
	headers := cr.fetcher.stripHeaders(ctx, remote, req.GetHeaders())
	for _, m := range cr.mutators {
		headers = m.Apply(ctx, remote, headers)
	}
	hash, err := cr.hasher.GetHash(remote, headers)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
		return resp.Send(&reactor.GetObjectV2Response{
			Content: nil,
			Code:    http.StatusInternalServerError,
		})
	}
	storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(refractName), req.Path, hash)
	// clone our data so that we can send it back to
	// the client and store it in the cache
	var buf bytes.Buffer
	tee := io.TeeReader(res.Response.Body, &buf)
	// upload the object to the cache
	if err := cr.provider.PutObject(ctx, storagePath, ioutil.NopCloser(tee)); err != nil {
		return resp.Send(&reactor.GetObjectV2Response{
			Content: nil,
			Code:    http.StatusInternalServerError,
		})
	}
	// verify that the data was inserted correctly
	if actualSize, err := cr.provider.GetSize(ctx, storagePath); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to read size of stored file")
	} else if actualSize.GetSize() != res.Response.ContentLength {
		// if the data isn't the expected size, delete it so that
		// we can avoid creating a corrupted cache.
		// this is partially mitigated in doCache, however we should
		// do our best to not put junk in the cache
		log.WithContext(ctx).Infof("detected size mismatch; expected cache size: %d, actually got: %d", res.Response.ContentLength, actualSize.GetSize())
		log.WithError(cr.provider.DeleteObject(ctx, storagePath)).Warning("attempting to cleanup corrupt data entry")
		metricCacheCorruptPut.Inc()
	}
	cr.createCacheEntry(ctx, remote.GetId(), refractID, req.Path, hash)
	// stream the response back
	return cr.streamResponse(ctx, &buf, http.StatusOK, resp)
}

// createCacheEntry contacts the API and stores
// metadata about a piece of the cache.
func (cr *BaseControlRod) createCacheEntry(ctx context.Context, remoteID, refractID, path, hash string) {
	reqID := tracer.GetContextId(ctx)
	fields := log.Fields{"id": reqID}
	log.WithContext(ctx).WithFields(fields).Infof("creating cache record for remote: %s", remoteID)
	resp, err := cr.cacheClient.Create(utils.SubmitCtx(ctx), &v1.CreateEntryRequest{
		Uri:       path,
		RemoteID:  remoteID,
		RefractID: refractID,
		Hash:      hash,
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to create CacheEntry")
		return
	}
	log.WithContext(ctx).WithFields(fields).Debugf("created entry record with id: %s", resp.Entry.Id)
}

// streamResponse handles the chunking and sending of
// data back to the API over the gRPC connection.
func (*BaseControlRod) streamResponse(ctx context.Context, data io.Reader, code int32, resp reactor.Reactor_GetObjectV2Server) error {
	// stream the file back over the rpc connection
	// buffer size is magic number found here - https://ops.tips/blog/sending-files-via-grpc/
	var buf = make([]byte, 1024)
	for {
		n, err := data.Read(buf)
		if err != nil || errors.Is(err, io.EOF) {
			log.WithError(err).WithContext(ctx).Error("failed to read chunk, or we've reached the end")
			break
		}
		if err := resp.Send(&reactor.GetObjectV2Response{
			// only send what we have
			Content: buf[:n],
			Code:    code,
		}); err != nil {
			log.WithError(err).WithContext(ctx).Error("failed to return chunk")
			return err
		}
	}
	return nil
}
