package reactor

import (
	"context"
	_ "embed"
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
	"net/http/httptest"
	"testing"
)

var defaultProfile = &v1.ClientProfile{}

func TestNewClient(t *testing.T) {
	assert.NotNil(t, NewClient(defaultProfile))
}

func TestClient_createRequest(t *testing.T) {
	c := NewClient(defaultProfile)
	req, err := c.createRequest(context.TODO(), &RetrieveObjectRequest{
		Headers: RequestHeaders{
			"X": "Y",
		},
		Path:   "http://localhost:0/test.txt",
		Method: http.MethodGet,
	})
	assert.NoError(t, err)
	assert.Equal(t, http.MethodGet, req.Method)
	assert.Equal(t, http.Header{"X": []string{"Y"}}, req.Header)
}

func TestClient_Do_BadMethodRejected(t *testing.T) {
	c := NewClient(defaultProfile)
	res := make(chan *RetrieveObjectResult)
	go c.Do(context.TODO(), &RetrieveObjectRequest{
		Headers: nil,
		Path:    "",
		Method:  "",
	}, res)
	resp := <-res

	assert.Nil(t, resp.Response)
	assert.ErrorIs(t, resp.Error, ErrInvalidMethod)
}

func TestClient_Do_ExecuteErr(t *testing.T) {
	c := NewClient(defaultProfile)
	res := make(chan *RetrieveObjectResult)
	go c.Do(context.TODO(), &RetrieveObjectRequest{
		Headers: nil,
		Path:    "http://localhost:0",
		Method:  http.MethodHead,
	}, res)
	resp := <-res

	assert.Nil(t, resp.Response)
	assert.Error(t, resp.Error)
}

func TestClient_Do_ErrorCode(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte("bad request"))
	}))
	defer ts.Close()

	c := NewClient(defaultProfile)
	res := make(chan *RetrieveObjectResult)
	go c.Do(context.TODO(), &RetrieveObjectRequest{
		Headers: nil,
		Path:    ts.URL,
		Method:  http.MethodHead,
	}, res)
	resp := <-res

	assert.NotNil(t, resp.Response)
	assert.ErrorIs(t, resp.Error, ErrNotOK)
}

func TestClient_Do(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	defer ts.Close()

	c := NewClient(defaultProfile)
	res := make(chan *RetrieveObjectResult)
	go c.Do(context.TODO(), &RetrieveObjectRequest{
		Headers: nil,
		Path:    ts.URL,
		Method:  http.MethodHead,
	}, res)
	resp := <-res

	assert.NotNil(t, resp.Response)
	assert.NoError(t, resp.Error)
}
