package reactor

import (
	"context"
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/policy"
	"net/http"
	"testing"
)

var policies, _ = policy.NewRegexRemotePolicies([]*v1.Remote{
	{Id: "0"},
	{Id: "1"},
	{Id: "2"},
	{Id: "3"},
})

func TestFetcher_getRemoteByID(t *testing.T) {
	cr := &Fetcher{
		policies: policies,
	}
	assert.NotNil(t, cr.getRemoteByID("0"))
	assert.NotNil(t, cr.getRemoteByID("1"))
	assert.NotNil(t, cr.getRemoteByID("2"))
	assert.NotNil(t, cr.getRemoteByID("3"))
	assert.Nil(t, cr.getRemoteByID("4"))
}

func TestFetcher_stripHeaders(t *testing.T) {
	var stripHeaderTests = []struct {
		name   string
		remote *v1.Remote
		in     RequestHeaders
		out    RequestHeaders
	}{
		{
			"Default headers are removed",
			&v1.Remote{StripRestricted: true, ClientProfile: defaultProfile},
			RequestHeaders{
				"Authorization": "Basic hunter2",
				"Job-Token":     "hunter2",
			},
			RequestHeaders{},
		},
		{
			"Default headers are ignored",
			&v1.Remote{StripRestricted: false, ClientProfile: defaultProfile},
			RequestHeaders{
				"Authorization": "Basic hunter2",
				"Foo":           "bar",
			},
			RequestHeaders{
				"Authorization": "Basic hunter2",
				"Foo":           "bar",
			},
		},
		{
			"Custom headers are removed",
			&v1.Remote{StripRestricted: true, RestrictedHeaders: []string{"Foo"}, ClientProfile: defaultProfile},
			RequestHeaders{
				"Authorization": "Basic hunter2",
				"Job-Token":     "hunter2",
				"Foo":           "Bar",
				"Abc":           "xyz",
			},
			RequestHeaders{
				"Abc": "xyz",
			},
		},
	}
	cr := &Fetcher{}
	for _, tt := range stripHeaderTests {
		t.Run(tt.name, func(t *testing.T) {
			headers := cr.stripHeaders(context.TODO(), tt.remote, tt.in)
			assert.Equal(t, tt.out, headers)
		})
	}
}

func TestFetcher_watchRequest(t *testing.T) {
	var watchRequestTests = []struct {
		name       string
		submitFunc func(chan *RetrieveObjectResult)
		eval       func(t *testing.T, res, fail *RetrieveObjectResult, id string)
	}{
		{
			"no successes",
			func(r chan *RetrieveObjectResult) {
				r <- &RetrieveObjectResult{
					Context:  nil,
					Request:  nil,
					Response: nil,
					Error:    ErrNotOK,
				}
				r <- &RetrieveObjectResult{
					Context:  nil,
					Request:  nil,
					Response: nil,
					Error:    ErrInvalidMethod,
				}
			},
			func(t *testing.T, res, fail *RetrieveObjectResult, id string) {
				assert.NotNil(t, res)
				assert.Nil(t, fail)
				assert.EqualValues(t, "", id)
			},
		},
		{
			"explicit failure",
			func(r chan *RetrieveObjectResult) {
				r <- &RetrieveObjectResult{
					Context:  nil,
					Request:  nil,
					Response: nil,
					Error:    ErrNotOK,
				}
				r <- &RetrieveObjectResult{
					Context:  nil,
					Request:  nil,
					Response: &http.Response{StatusCode: http.StatusNotFound},
					Error:    ErrNotOK,
				}
			},
			func(t *testing.T, res, fail *RetrieveObjectResult, id string) {
				assert.NotNil(t, res)
				assert.NotNil(t, fail)
				assert.Equal(t, http.StatusNotFound, fail.Response.StatusCode)
				assert.EqualValues(t, "", id)
			},
		},
		{
			"success on 2nd request",
			func(r chan *RetrieveObjectResult) {
				r <- &RetrieveObjectResult{
					Context:  context.WithValue(context.TODO(), ContextKeyID, "0"),
					Request:  nil,
					Response: &http.Response{StatusCode: http.StatusNotFound},
					Error:    ErrNotOK,
				}
				r <- &RetrieveObjectResult{
					Context:  context.WithValue(context.TODO(), ContextKeyID, "1"),
					Request:  nil,
					Response: &http.Response{StatusCode: http.StatusOK},
					Error:    nil,
				}
			},
			func(t *testing.T, res, fail *RetrieveObjectResult, id string) {
				assert.NotNil(t, res)
				assert.NotNil(t, fail)
				assert.EqualValues(t, "1", id)
			},
		},
		{
			"success on 1st request",
			func(r chan *RetrieveObjectResult) {
				r <- &RetrieveObjectResult{
					Context:  context.WithValue(context.TODO(), ContextKeyID, "1"),
					Request:  nil,
					Response: &http.Response{StatusCode: http.StatusOK},
					Error:    nil,
				}
				r <- &RetrieveObjectResult{
					Context:  context.WithValue(context.TODO(), ContextKeyID, "0"),
					Request:  nil,
					Response: &http.Response{StatusCode: http.StatusNotFound},
					Error:    ErrNotOK,
				}
			},
			func(t *testing.T, res, fail *RetrieveObjectResult, id string) {
				assert.NotNil(t, res)
				assert.Nil(t, fail)
				assert.EqualValues(t, "1", id)
			},
		},
	}
	cr := &Fetcher{}
	ctxMap := contextMap{
		"0": func() {},
		"1": func() {},
	}
	for _, tt := range watchRequestTests {
		t.Run(tt.name, func(t *testing.T) {
			res := make(chan *RetrieveObjectResult)
			go tt.submitFunc(res)
			result, failure, remoteID := cr.watchRequest(context.TODO(), res, ctxMap)
			tt.eval(t, result, failure, remoteID)
		})
	}
}
