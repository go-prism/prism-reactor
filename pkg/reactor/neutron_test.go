package reactor

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewNeutron(t *testing.T) {
	n, err := NewNeutron(nil, nil, nil, nil, nil)
	assert.NoError(t, err)
	assert.NotNil(t, n)
}
