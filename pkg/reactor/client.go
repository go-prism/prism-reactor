package reactor

import (
	"context"
	"errors"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/djcass44/go-utils/pkg/sliceutils"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/httpclient"
	"net/http"
	"time"
)

var (
	// ErrNotOK indicates that the client successfully executed
	// the request, however it returned a non-successful status code.
	ErrNotOK = errors.New("client received an unexpected status code")
	// ErrMissingBody indicates that the client successfully executed
	// the request, however the response body was empty.
	ErrMissingBody = errors.New("client received a response with zero content")
	// ErrInvalidMethod indicates that the client has refused to
	// proxy a request for an invalid http method.
	//
	// Supported methods are:
	//
	// * GET
	//
	// * HEAD
	//
	// * TRACE
	ErrInvalidMethod = errors.New("client rejected request for unsupported http method")
	// ErrCacheMissed indicates that the reactor was unable
	// to find a cached version of the request object.
	ErrCacheMissed = errors.New("cache entry could not be found")
	allowedMethods = []string{
		http.MethodGet,
		http.MethodHead,
		http.MethodTrace,
	}
)

type Client struct {
	client *http.Client
}

func NewClient(profile *v1.ClientProfile) *Client {
	c := new(Client)
	c.client = httpclient.NewClientBuilder().
		FromProfile(profile).
		Build()
	tracer.Apply(c.client)

	return c
}

func (c *Client) createRequest(ctx context.Context, req *RetrieveObjectRequest) (*http.Request, error) {
	id := tracer.GetContextId(ctx)
	log.WithFields(log.Fields{
		"id":     id,
		"method": req.Method,
		"path":   req.Path,
	}).Debug("preparing request")
	request, err := http.NewRequestWithContext(ctx, req.Method, req.Path, nil)
	if err != nil {
		log.WithError(err).Error("failed to create request")
		return nil, err
	}
	// copy the headers
	for k, v := range req.Headers {
		request.Header.Add(k, v)
	}
	return request, nil
}

func (c *Client) Do(ctx context.Context, req *RetrieveObjectRequest, res chan *RetrieveObjectResult) {
	id := tracer.GetContextId(ctx)
	// check that the request is using an allowed method.
	//
	// this is to stop us from attempting to write to a remote
	// as they should be R/O
	if req.Headers[HeaderAlwaysProxy] != "true" && !sliceutils.Includes(allowedMethods, req.Method) {
		log.WithError(ErrInvalidMethod).WithContext(ctx).WithFields(log.Fields{
			"id":     id,
			"method": req.Method,
			"path":   req.Path,
		}).Error("unsupported method")
		res <- &RetrieveObjectResult{
			Context:  ctx,
			Request:  req,
			Response: nil,
			Error:    ErrInvalidMethod,
		}
		return
	}
	request, err := c.createRequest(ctx, req)
	if err != nil {
		res <- &RetrieveObjectResult{
			Context:  ctx,
			Request:  req,
			Response: nil,
			Error:    err,
		}
		return
	}
	log.WithContext(ctx).WithFields(log.Fields{
		"id":     id,
		"method": req.Method,
		"path":   req.Path,
	}).Info("executing request")
	start := time.Now()
	// execute the request
	resp, err := c.client.Do(request)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(log.Fields{
			"id":     id,
			"method": req.Method,
			"path":   req.Path,
		}).Error("failed to execute request")
		res <- &RetrieveObjectResult{
			Context:  ctx,
			Request:  req,
			Response: nil,
			Error:    err,
		}
		return
	}
	log.WithContext(ctx).WithFields(log.Fields{
		"id":   id,
		"code": resp.StatusCode,
		"path": req.Path,
	}).Infof("request responded in %s", time.Since(start))
	// we're expecting a 200 so return an error if we don't get it
	if httputils.IsHTTPError(resp.StatusCode) {
		res <- &RetrieveObjectResult{
			Context:  ctx,
			Request:  req,
			Response: resp,
			Error:    ErrNotOK,
		}
		return
	}
	// we need content in the response, so bail out
	// unless it was a HEAD request
	if req.Method != http.MethodHead {
		if resp.ContentLength <= 0 {
			res <- &RetrieveObjectResult{
				Context:  ctx,
				Request:  req,
				Response: resp,
				Error:    ErrMissingBody,
			}
			return
		}
	}
	// return the raw response and let the
	// control rod figure out what to do with it
	res <- &RetrieveObjectResult{
		Context:  ctx,
		Request:  req,
		Response: resp,
		Error:    nil,
	}
}
