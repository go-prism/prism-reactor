package reactor

import (
	"context"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"net/http"
)

var (
	// HeaderAlwaysProxy indicates that the client
	// should ignore the METHOD and proxy the request
	// no matter what.
	HeaderAlwaysProxy = http.CanonicalHeaderKey("X-Prism-Always-Proxy")
	// ErrMissingRemote indicates that a client is asking for
	// data from a remote that doesn't exist
	ErrMissingRemote = status.Error(codes.NotFound, "remote not found")
	// ErrBlockedByPolicy indicates that a policy has blocked
	// the requested path or file.
	ErrBlockedByPolicy = status.Error(codes.PermissionDenied, "request blocked by remote policy")
)

type contextKey int
type RequestHeaders map[string]string
type contextMap map[string]context.CancelFunc

type RetrieveObjectRequest struct {
	Headers RequestHeaders
	Path    string
	Method  string
}

type RetrieveObjectResult struct {
	Context  context.Context
	Request  *RetrieveObjectRequest
	Response *http.Response
	Error    error
}

type DataStore interface {
	Do(ctx context.Context, req *reactor.GetObjectRequest, resp reactor.Reactor_GetObjectV2Server) error
	ListObjects(ctx context.Context, prefix string) ([]string, error)
	HasObject(ctx context.Context, req *reactor.GetObjectRequest) (bool, error)
	PutObject(ctx context.Context, req *reactor.GetObjectRequest, r io.Reader) error
}
