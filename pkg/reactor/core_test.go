package reactor

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.dcas.dev/prism/prism-reactor/internal/testutils"
	"net/http"
	"net/http/httptest"
	"testing"
)

func getTestRodPool(t *testing.T) *RodPool {
	rp, err := NewRodPool(
		&testutils.TestReactorClient{
			Srv: httptest.NewServer(http.NotFoundHandler()),
		},
		testutils.NewTestProvider(),
		&testutils.TestClient{},
		&testutils.TestRemotesClient{},
		h,
		nil,
	)
	assert.NoError(t, err)
	return rp
}

func TestNewRodPool(t *testing.T) {
	rp, err := NewRodPool(nil, nil, nil, &testutils.TestRemotesClient{}, h, nil)
	assert.NoError(t, err)
	assert.NotNil(t, rp)
}

func TestRodPool_Get(t *testing.T) {
	testRodPool := getTestRodPool(t)

	cr, err := testRodPool.Get(context.TODO(), "test")
	assert.NoError(t, err)

	cr2, err := testRodPool.Get(context.TODO(), "test")
	assert.NoError(t, err)

	assert.Equal(t, cr, cr2)
}

func TestRodPool_GetError(t *testing.T) {
	testRodPool := getTestRodPool(t)

	_, err := testRodPool.Get(context.TODO(), testutils.MagicErrKey)
	assert.Error(t, err)
}

func TestRodPool_GetNeutron(t *testing.T) {
	testRodPool := getTestRodPool(t)

	cr, err := testRodPool.Get(context.TODO(), testutils.Magic404Key)
	assert.NoError(t, err)
	assert.NotNil(t, cr)
}

func TestRodPool_Keys(t *testing.T) {
	testRodPool := getTestRodPool(t)

	assert.Empty(t, testRodPool.Keys())
	_, err := testRodPool.Get(context.TODO(), "test")
	assert.NoError(t, err)
	assert.ElementsMatch(t, []string{"test"}, testRodPool.Keys())
}

func TestRodPool_Delete(t *testing.T) {
	testRodPool := getTestRodPool(t)

	ok := testRodPool.Delete(context.TODO(), "test")
	assert.False(t, ok)
	_, err := testRodPool.Get(context.TODO(), "test")
	assert.NoError(t, err)

	ok = testRodPool.Delete(context.TODO(), "test")
	assert.True(t, ok)
}
