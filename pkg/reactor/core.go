package reactor

import (
	"context"
	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/pkg/utils"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache/partition"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type RodPool struct {
	rec           v1.ReactorClient
	provider      cache.Provider
	pool          map[string]DataStore
	cacheClient   v1.CacheClient
	hasher        hasher.Hasher
	remotesClient api.RemotesClient
	neutron       *Neutron
	mutators      []partition.Mutator
}

func NewRodPool(rec v1.ReactorClient, p cache.Provider, cacheClient v1.CacheClient, remotesClient api.RemotesClient, hasher hasher.Hasher, mutators []partition.Mutator) (*RodPool, error) {
	rp := new(RodPool)
	rp.rec = rec
	rp.provider = p
	rp.pool = map[string]DataStore{}
	rp.cacheClient = cacheClient
	rp.hasher = hasher
	rp.remotesClient = remotesClient
	neutron, err := NewNeutron(nil, p, cacheClient, hasher, mutators)
	if err != nil {
		return nil, err
	}
	rp.neutron = neutron
	rp.init(context.Background())

	return rp, nil
}

func (rp *RodPool) Keys() []string {
	//goland:noinspection GoPreferNilSlice
	keys := []string{}
	for k := range rp.pool {
		keys = append(keys, k)
	}
	return keys
}

func (rp *RodPool) init(ctx context.Context) {
	log.WithContext(ctx).Info("refreshing Neutron remotes")
	remotes, err := rp.remotesClient.List(ctx, &api.ListRequest{Archetype: ""})
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to fetch remotes for Neutron")
		return
	}
	if err = rp.neutron.fetcher.Refresh(remotes.GetRemotes()); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to refresh Neutron remotes")
		return
	}
}

func (rp *RodPool) Purge(ctx context.Context) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{"id": id}
	log.WithFields(fields).Warning("purging reactor")
	metricControlPurge.Inc()
	metricControlGauge.Set(0)
	rp.pool = map[string]DataStore{}
	rp.init(ctx)
}

func (rp *RodPool) Delete(ctx context.Context, k string) bool {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{"id": id, "key": k}
	log.WithFields(fields).Info("removing control rod from pool")
	_, ok := rp.pool[k]
	if !ok {
		log.WithFields(fields).Debug("failed to locate control rod")
		return false
	}
	metricControlDelete.WithLabelValues(k).Inc()
	metricControlGauge.Dec()
	delete(rp.pool, k)
	return true
}

func (rp *RodPool) Get(ctx context.Context, k string) (DataStore, error) {
	fields := log.Fields{"key": k}
	r, ok := rp.pool[k]
	if ok {
		metricControlGetHit.WithLabelValues(k).Inc()
		log.WithContext(ctx).WithFields(fields).Debug("found existing control rod")
		return r, nil
	}
	log.WithContext(ctx).WithFields(fields).Info("couldn't find existing control rod, we will need to start a new one")
	// fetch the remotes and whatnot
	ctxData, err := rp.rec.Get(utils.SubmitCtx(ctx), &v1.GetContextRequest{
		Key: k,
	})
	if err != nil {
		if s, ok := status.FromError(err); ok && s.Code() == codes.NotFound {
			log.WithContext(ctx).WithFields(fields).Infof("failed to find refraction with key: '%s', assuming it's a remote", k)
			log.WithContext(ctx).WithFields(fields).Infof("successfully started neutron control rod '%s'", k)
			return rp.neutron, nil
		}
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to get control rod contextual information")
		return nil, err
	}
	cr, err := NewControlRod(ctxData.GetRefraction(), ctxData.GetRemotes(), rp.provider, rp.cacheClient, rp.hasher, rp.mutators)
	if err != nil {
		return nil, err
	}
	log.WithContext(ctx).WithFields(fields).Infof("successfully started control rod '%s'", k)
	metricControlGetMiss.WithLabelValues(k).Inc()
	metricControlGauge.Inc()
	rp.pool[k] = cr
	return cr, nil
}
