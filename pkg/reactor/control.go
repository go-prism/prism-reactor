package reactor

import (
	"context"
	"fmt"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache/partition"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	daov1 "gitlab.com/go-prism/prism-api/pkg/dto/v1"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/cache"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/policy"
)

const ContextKeyID contextKey = iota

var (
	// DefaultRestrictedHeaders provides a list of
	// known authentication headers which should be protected
	DefaultRestrictedHeaders = []string{
		http.CanonicalHeaderKey("Authorization"),
		http.CanonicalHeaderKey("Private-Token"),
		http.CanonicalHeaderKey("Deploy-Token"),
		http.CanonicalHeaderKey("Job-Token"),
	}
)

// ControlRod provides a method of fetching data
// from a Refraction.
type ControlRod struct {
	base     *BaseControlRod
	refract  *v1.Refraction
	provider cache.Provider
	hasher   hasher.Hasher
	fetcher  *Fetcher
	neutron  *Neutron
}

// NewControlRod creates an instance of ControlRod
func NewControlRod(refract *v1.Refraction, remotes []*v1.Remote, p cache.Provider, cacheClient v1.CacheClient, hasher hasher.Hasher, mutators []partition.Mutator) (*ControlRod, error) {
	cr := new(ControlRod)
	cr.refract = refract
	cr.provider = p
	cr.base = &BaseControlRod{
		hasher:      hasher,
		cacheClient: cacheClient,
		provider:    p,
		mutators:    mutators,
	}
	cr.hasher = hasher
	neutron, err := NewNeutron(remotes, p, cacheClient, hasher, mutators)
	if err != nil {
		return nil, err
	}
	cr.neutron = neutron
	// setup the fetcher
	fetcher, err := NewFetcher(remotes)
	if err != nil {
		return nil, err
	}
	cr.fetcher = fetcher
	cr.base.fetcher = fetcher
	return cr, nil
}

// doCache handles cache interaction before querying the remote.
//
// 1. find the remotes that are allowed/able to service this query
// 2. check if the cache is holding the file
// 3. stream the file back to the client if we found it
//
// this function must return an error to indicate that the file was not retrieved
// from the cache.
func (cr *ControlRod) doCache(ctx context.Context, req *reactor.GetObjectRequest, resp reactor.Reactor_GetObjectV2Server) error {
	reqID := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id":   reqID,
		"path": req.Path,
	}
	// get the remotes that match our allowed policies
	remotes := policy.GetMatchingRemotes(req.Path, cr.fetcher.policies)
	log.WithContext(ctx).WithFields(fields).Infof("checking cache for existing object in %d matching remotes", len(remotes))
	for _, r := range remotes {
		remote := r.GetRemote()
		if !r.CanCache(req.Path) {
			log.WithContext(ctx).WithFields(fields).Debugf("skipping cache check for remote on path: '%s'", req.Path)
			continue
		}
		headers := cr.fetcher.stripHeaders(ctx, remote, req.GetHeaders())
		for _, m := range cr.base.mutators {
			headers = m.Apply(ctx, remote, headers)
		}
		// get the header-hash so that we can check the
		// appropriate cache partition
		hash, err := cr.hasher.GetHash(remote, headers)
		if err != nil {
			log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
			return err
		}
		storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(cr.refract.GetName()), req.Path, hash)
		if ok, err := cr.provider.HasObject(ctx, storagePath); err != nil || !ok {
			log.WithError(err).WithContext(ctx).WithFields(fields).Errorf("failed to find existing object in remote: '%s'", remote.GetName())
			continue
		}
		log.WithContext(ctx).WithFields(fields).Infof("found cache entry in remote: '%s'", remote.GetName())
		// confirm that the file is not 0 bytes.
		// sometimes failed downloads cache an empty file
		// which causes issues for client such as Maven
		if info, err := cr.provider.GetSize(ctx, storagePath); err != nil {
			log.WithError(err).WithContext(ctx).WithFields(log.Fields{
				"remote": remote.GetName(),
				"path":   storagePath,
			}).Error("failed to read for cached object")
			continue
		} else if info.Size <= 0 {
			log.WithContext(ctx).WithFields(log.Fields{
				"remote": remote.GetName(),
				"path":   storagePath,
			}).Warning("detected cached file with size of 0")
			metricCacheCorruptGet.Inc()
			continue
		}
		cr.base.createCacheEntry(ctx, remote.GetId(), cr.refract.GetId(), req.Path, hash)
		// download the file
		data, err := cr.provider.StreamGetObject(ctx, storagePath)
		if err != nil {
			return err
		}
		// stream the file back over the rpc connection
		//
		// 2021/06/23 - sending a 304 causes an error since
		// you're not allowed to send a body with it :(
		return cr.base.streamResponse(ctx, data, http.StatusOK, resp)
	}
	return ErrCacheMissed
}

// Do executes a proxied request
func (cr *ControlRod) Do(ctx context.Context, req *reactor.GetObjectRequest, resp reactor.Reactor_GetObjectV2Server) error {
	fields := log.Fields{
		"path":   req.Path,
		"method": req.Method,
	}
	// check the cache first
	err := cr.doCache(ctx, req, resp)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("unable to find cache entry, we will insert the control rod")
	} else {
		return nil
	}
	if cr.refract.GetArchetype() == daov1.ArchetypeGo {
		log.WithContext(ctx).WithFields(fields).Warning("golang controlrod was unable to find cached data, we will return here so the plugin can handle it")
		return resp.Send(&reactor.GetObjectV2Response{
			Content: nil,
			Code:    http.StatusNotFound,
		})
	}
	log.WithContext(ctx).WithFields(fields).Info("inserting control rod")
	defer func() {
		log.WithContext(ctx).WithFields(fields).Info("removing control rod")
	}()

	// find the best candidate to retrieve the data from
	remote, code, err := cr.fetcher.GetCandidate(ctx, req)
	if err != nil {
		return resp.Send(&reactor.GetObjectV2Response{
			Content: nil,
			Code:    int32(code),
		})
	}
	return cr.base.fetch(ctx, remote.GetRemote(), cr.refract.GetName(), cr.refract.GetId(), req, resp, func() *RetrieveObjectResult {
		return cr.fetcher.Get(ctx, req, remote)
	})
}

func (cr *ControlRod) ListObjects(ctx context.Context, prefix string) ([]string, error) {
	storagePath := fmt.Sprintf("%s/%s", strings.ToLower(cr.refract.GetName()), prefix)
	log.WithContext(ctx).Infof("listing objects under path: '%s'", storagePath)
	return cr.provider.ListObjects(ctx, storagePath)
}

func (cr *ControlRod) HasObject(ctx context.Context, req *reactor.GetObjectRequest) (bool, error) {
	hash, err := cr.hasher.GetHash(&v1.Remote{RestrictedHeaders: DefaultRestrictedHeaders}, req.Headers)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
		return false, err
	}
	storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(cr.refract.GetName()), req.Path, hash)
	log.WithContext(ctx).Infof("check for existance of object at path: '%s'", storagePath)
	return cr.provider.HasObject(ctx, storagePath)
}

func (cr *ControlRod) PutObject(ctx context.Context, req *reactor.GetObjectRequest, r io.Reader) error {
	hash, err := cr.hasher.GetHash(&v1.Remote{RestrictedHeaders: DefaultRestrictedHeaders}, req.Headers)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to generate hash, check your master key")
		return err
	}
	storagePath := fmt.Sprintf("%s/%s/%s", strings.ToLower(cr.refract.GetName()), req.Path, hash)
	log.WithContext(ctx).Infof("uploading object to path: '%s'", storagePath)
	// upload the object to the cache
	if err := cr.provider.PutObject(ctx, storagePath, ioutil.NopCloser(r)); err != nil {
		return err
	}
	if len(cr.refract.GetRemotes()) > 0 {
		cr.base.createCacheEntry(ctx, cr.refract.GetRemotes()[0], cr.refract.GetId(), req.Path, "")
	}
	return nil
}
