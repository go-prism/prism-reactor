package reactor

import (
	"context"
	v12 "gitlab.com/go-prism/prism-api/pkg/dto/v1"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/hasher"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/internal/testutils"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

var h = hasher.NewScrypt("hunter2", DefaultRestrictedHeaders)

func TestNewControlRod(t *testing.T) {
	cr, err := NewControlRod(nil, nil, nil, nil, nil, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cr)
}

func TestControlRod_Do(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	}))
	t2 := httptest.NewServer(http.NotFoundHandler())
	defer ts.Close()
	defer t2.Close()

	cr, err := NewControlRod(&v1.Refraction{
		Name: "test",
	}, []*v1.Remote{
		{
			Name:          "test1",
			Uri:           ts.URL,
			ClientProfile: defaultProfile,
			Archetype:     v12.ArchetypeGeneric,
		},
		{
			Name:      "test2",
			Uri:       t2.URL,
			Enabled:   wrapperspb.Bool(false), // this remote should be skipped
			Archetype: v12.ArchetypeGeneric,
		},
	}, testutils.NewTestProvider(), &testutils.TestClient{}, h, nil)
	assert.NoError(t, err)
	req := &reactor.GetObjectRequest{
		Headers: nil,
		Path:    "/test.txt",
		Method:  http.MethodGet,
	}
	res := &testutils.TestReactorStream{
		T:               t,
		ExpectedCode:    http.StatusOK,
		ExpectedContent: []byte("OK"),
	}
	err = cr.Do(context.TODO(), req, res)
	assert.NoError(t, err)

	t.Log("FETCHING A 2ND TIME")
	// stop the server so that the cache must give us our data
	ts.Close()
	// fetch it a 2nd time and confirm we received the data
	err = cr.Do(context.TODO(), req, res)
	assert.NoError(t, err)
}

func TestControlRod_doCache(t *testing.T) {
	cr, err := NewControlRod(&v1.Refraction{
		Name: "test",
	}, []*v1.Remote{
		{
			Name:          "test",
			Uri:           "",
			ClientProfile: defaultProfile,
			Archetype:     "maven",
		},
	}, testutils.NewTestProvider(), &testutils.TestClient{}, h, nil)
	assert.NoError(t, err)
	assert.NotNil(t, cr)

	var cases = []struct {
		name string
		path string
	}{
		{
			"missing file returns error",
			"",
		},
		{
			"corrupt file returns error",
			testutils.TestPathEmpty,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			err = cr.doCache(context.TODO(), &reactor.GetObjectRequest{
				Headers: RequestHeaders{},
				Path:    tt.path,
				Method:  http.MethodGet,
			}, nil)
			assert.ErrorIs(t, err, ErrCacheMissed)
		})
	}
}
