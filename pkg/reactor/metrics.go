package reactor

import "github.com/prometheus/client_golang/prometheus"

var (
	metricControlDelete = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_delete",
	}, []string{"controlrod"})
	metricControlGetHit = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_get_hit",
	}, []string{"controlrod"})
	metricControlGetMiss = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_get_miss",
	}, []string{"controlrod"})
	metricControlPurge = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_purge",
	})
	metricControlGauge = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_active",
	})
	metricCacheCorruptGet = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_cache_corrupt_get",
		Help:      "Total number of corrupt files detected in the cache (e.g. 0 bytes) before being retrieved.",
	})
	metricCacheCorruptPut = prometheus.NewCounter(prometheus.CounterOpts{
		Namespace: "prism",
		Subsystem: "reactor",
		Name:      "controlrod_cache_corrupt_put",
		Help:      "Total number of corrupt files detected in the cache (e.g. 0 bytes) after being inserted.",
	})
)
