package reactor

import (
	"context"
	"fmt"
	"github.com/djcass44/go-utils/pkg/sliceutils"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/policy"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"math"
	"net/http"
	"strings"
	"time"
)

type Fetcher struct {
	policies []policy.RemotePolicy
	clients  map[string]*Client
}

func NewFetcher(remotes []*v1.Remote) (*Fetcher, error) {
	f := new(Fetcher)
	f.clients = map[string]*Client{}
	err := f.Refresh(remotes)
	if err != nil {
		return nil, err
	}

	return f, nil
}

func (f *Fetcher) Refresh(remotes []*v1.Remote) error {
	// generate the remote policies
	policies, err := policy.NewRegexRemotePolicies(remotes)
	if err != nil {
		return err
	}
	f.policies = policies
	return nil
}

func (*Fetcher) stripHeaders(ctx context.Context, r *v1.Remote, h RequestHeaders) RequestHeaders {
	fields := log.Fields{"remote": r.GetName()}
	if !r.GetStripRestricted() {
		log.WithContext(ctx).WithFields(fields).Debug("skipping header stripping")
		return h
	}
	headers := RequestHeaders{}
	restricted := append(r.RestrictedHeaders, DefaultRestrictedHeaders...)
	for k, v := range h {
		if !sliceutils.Includes(restricted, k) {
			headers[k] = v
		} else {
			log.WithContext(ctx).WithFields(fields).Debugf("stripping restricted header: %s", k)
		}
	}
	return headers
}

func (f *Fetcher) getRemoteByID(id string) policy.RemotePolicy {
	for _, r := range f.policies {
		if r.GetRemote().GetId() == id {
			return r
		}
	}
	return nil
}

// getRemoteByName fetches a remote by its name.
// Not case-sensitive.
func (f *Fetcher) getRemoteByName(name string) (policy.RemotePolicy, bool) {
	n := strings.ToLower(name)
	for _, r := range f.policies {
		if strings.ToLower(r.GetRemote().GetName()) == n {
			return r, true
		}
	}
	return nil, false
}

// GetByName retrieves a file from a remote. The remote
// if determined by the given name.
func (f *Fetcher) GetByName(ctx context.Context, req *reactor.GetObjectRequest, remoteName string) *RetrieveObjectResult {
	remote, ok := f.getRemoteByName(remoteName)
	if !ok {
		log.WithContext(ctx).Warningf("failed to find remote with name: '%s'", remoteName)
		return &RetrieveObjectResult{
			Context: ctx,
			Request: &RetrieveObjectRequest{
				Headers: req.GetHeaders(),
				Path:    req.GetPath(),
				Method:  req.GetMethod(),
			},
			Response: nil,
			Error:    ErrMissingRemote,
		}
	}
	return f.Get(ctx, req, remote)
}

// Get retrieves a file from a given remote.
func (f *Fetcher) Get(ctx context.Context, req *reactor.GetObjectRequest, remote policy.RemotePolicy) *RetrieveObjectResult {
	// check that we're actually allowed to go to this file
	if !remote.CanReceive(req.Path) {
		log.WithContext(ctx).Warning("rejecting request since policy disallows it")
		return &RetrieveObjectResult{
			Context: ctx,
			Request: &RetrieveObjectRequest{
				Headers: req.GetHeaders(),
				Path:    req.GetPath(),
				Method:  req.GetMethod(),
			},
			Response: nil,
			Error:    ErrBlockedByPolicy,
		}
	}
	result := make(chan *RetrieveObjectResult)
	remoteReq := &RetrieveObjectRequest{
		Headers: f.stripHeaders(ctx, remote.GetRemote(), req.Headers),
		Path:    fmt.Sprintf("%s/%s", strings.TrimSuffix(strings.TrimSpace(remote.GetRemote().GetUri()), "/"), req.Path),
		Method:  req.Method,
	}
	log.WithContext(ctx).Infof("fetching object: %s", remoteReq.Path)
	go f.getClient(ctx, remote.GetRemote()).Do(ctx, remoteReq, result)
	res := <-result
	return res
}

// getClient retrieves a cached client
// or creates a new one.
//
// this may cause problems - see #6 for info
func (f *Fetcher) getClient(ctx context.Context, remote *v1.Remote) *Client {
	// try fetch a cached client
	client, ok := f.clients[remote.GetId()]
	if !ok {
		// otherwise, create a new one
		log.WithContext(ctx).Debugf("failed to find cached client for candidate: %s", remote.GetId())
		client = NewClient(remote.ClientProfile)
		// cache the client so we don't have to create it next time
		f.clients[remote.GetId()] = client
	}
	return client
}

// GetCandidate sends a head request to each possible remote
// and returns the first remote to get a successful remote.
func (f *Fetcher) GetCandidate(ctx context.Context, req *reactor.GetObjectRequest) (policy.RemotePolicy, int, error) {
	fields := log.Fields{
		"path":   req.Path,
		"method": req.Method,
	}
	result := make(chan *RetrieveObjectResult)
	// keep a map of contexts so we know which one to cancel
	ctxMap := contextMap{}

	// get the remotes that match our allowed policies
	remotes := policy.GetMatchingRemotes(req.Path, f.policies)
	log.WithContext(ctx).WithFields(fields).Debugf("targeting %d/%d remotes", len(remotes), len(f.policies))
	start := time.Now()

	var remoteReq *RetrieveObjectRequest
	var r *v1.Remote

	for _, rem := range remotes {
		r = rem.GetRemote()
		// skip the remote if it's explicitly disabled
		//
		// this can't be checked by the policy engine
		// because we still want it to be available
		// in the cache.
		if r.Enabled != nil && !r.Enabled.GetValue() {
			log.WithContext(ctx).WithFields(fields).Warningf("skipping disabled remote: '%s'", r.GetName())
			continue
		}
		ctx, cancel := context.WithCancel(context.WithValue(ctx, ContextKeyID, r.GetId()))
		ctxMap[r.GetId()] = cancel

		remoteReq = &RetrieveObjectRequest{
			Headers: f.stripHeaders(ctx, r, req.Headers),
			Path:    fmt.Sprintf("%s/%s", strings.TrimSuffix(strings.TrimSpace(r.GetUri()), "/"), req.Path),
			Method:  http.MethodHead,
		}
		go f.getClient(ctx, r).Do(ctx, remoteReq, result)
	}
	res, failure, responder := f.watchRequest(ctx, result, ctxMap)
	if res == nil || res.Error != nil {
		log.WithContext(ctx).WithFields(fields).Info("received final fail signal with no successful responses")
		if failure == nil {
			log.WithContext(ctx).WithFields(fields).Info("cannot find failed response to return, will return generic 404")
			return nil, http.StatusNotFound, status.Error(codes.NotFound, "not found")
		}
		// cleanup if we can
		log.WithContext(ctx).Infof("failed to get successful response from all remotes, responding with code: %d", failure.Response.StatusCode)
		_ = failure.Response.Body.Close()
		return nil, failure.Response.StatusCode, status.Errorf(codes.Unknown, "response failed with code: %d", failure.Response.StatusCode)
	}
	defer res.Response.Body.Close()
	// lookup the remote that responded
	remote := f.getRemoteByID(responder)
	if remote == nil {
		log.WithContext(ctx).Error("failed to find matching remote")
		return nil, http.StatusInternalServerError, status.Errorf(codes.Unknown, "failed to find remote with id: %s", responder)
	}
	log.WithContext(ctx).Infof("found best candidate: %s (%s) in %s", remote.GetRemote().GetId(), remote.GetRemote().GetName(), time.Since(start))
	return remote, res.Response.StatusCode, nil
}

// watchRequest watches a channel for Client responses
// and extracts the first success or final failure.
func (*Fetcher) watchRequest(ctx context.Context, c chan *RetrieveObjectResult, ctxMap contextMap) (res *RetrieveObjectResult, fail *RetrieveObjectResult, remoteID string) {
	remoteID = ""
	count := 0
	for count < len(ctxMap) {
		res = <-c
		// if we got a good response, stop watching
		if res.Response != nil && res.Error == nil {
			log.WithContext(ctx).Infof("found successful result after %d responses", count+1)
			count = math.MaxInt32
			// cancel the other requests
			id := res.Context.Value(ContextKeyID).(string)
			remoteID = id
			for k, v := range ctxMap {
				if k != id {
					log.WithContext(ctx).Debugf("cancelling request with id '%s'", k)
					v()
				}
			}
		} else {
			// store responses backwards so 1st is most recent
			if res.Response != nil {
				fail = res
				log.WithContext(ctx).Info("saving unsuccessful result in channel as fallback response")
			} else {
				log.WithContext(ctx).Info("found unsuccessful result in channel")
			}
		}
		count++
	}
	return res, fail, remoteID
}
