package policy_test

import (
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/policy"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"testing"
)

var centralRemote = &v1.Remote{
	Name:      "central",
	Uri:       "https://repo1.maven.org/maven2",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "maven",
	AllowList: nil,
	BlockList: []string{"^/(com.github).+"},
}
var jitRemote = &v1.Remote{
	Name:      "jitpack",
	Uri:       "https://jitpack.io",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "maven",
	AllowList: nil,
	BlockList: nil,
}

func TestGetMatchingRemotes(t *testing.T) {
	remotes, err := policy.NewRegexRemotePolicies([]*v1.Remote{
		centralRemote,
		jitRemote,
	})
	assert.NoError(t, err)
	matches := policy.GetMatchingRemotes("/io/rest-assured/rest-assured-common/3.3.0/rest-assured-common-3.3.0.pom", remotes)
	assert.ElementsMatch(t, remotes, matches)

	matches = policy.GetMatchingRemotes("/com/github/djcass44/jmp-security-utils/0.1.2-beta.3/jmp-security-utils-0.1.2-beta.3.pom", remotes)
	assert.ElementsMatch(t, []policy.RemotePolicy{remotes[1]}, matches)
}
