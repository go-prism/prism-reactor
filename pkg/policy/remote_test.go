package policy_test

import (
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/policy"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"testing"
)

var mavenRemote = &v1.Remote{
	Name:      "maven-test",
	Uri:       "https://repo1.maven.org/maven2",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "maven",
	AllowList: nil,
	BlockList: []string{"^/(com.github).+"},
}

var npmRemote = &v1.Remote{
	Name:      "npm",
	Uri:       "https://registry.npmjs.org",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "node",
	AllowList: nil,
	BlockList: nil,
}

var alpineRemote = &v1.Remote{
	Name:      "dl-cdn",
	Uri:       "https://dl-cdn.alpinelinux.org/alpine",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "alpine",
	AllowList: nil,
	BlockList: nil,
}

var genericRemote = &v1.Remote{
	Name:      "minio-local",
	Uri:       "http://localhost:9000/somebucket",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "generic",
	AllowList: nil,
	BlockList: nil,
}

var debianRemote = &v1.Remote{
	Name:      "minio-local",
	Uri:       "http://localhost:9000/somebucket",
	Enabled:   wrapperspb.Bool(true),
	Archetype: "debian",
	AllowList: nil,
	BlockList: nil,
}

func TestRegexRemotePolicy_GetRemote(t *testing.T) {
	rrp, err := policy.NewRegexRemotePolicy(mavenRemote)
	assert.NoError(t, err)
	assert.NotNil(t, rrp.GetRemote())
}

var recvTests = []struct {
	remote *v1.Remote
	in     string
	out    bool
}{
	{
		mavenRemote,
		"/io/rest-assured/rest-assured-common/3.3.0/rest-assured-common-3.3.0.pom",
		true,
	},
	{
		mavenRemote,
		"/com/github/djcass44/jmp-security-utils/0.1.2-beta.3/jmp-security-utils-0.1.2-beta.3.pom",
		false,
	},
}

func TestRegexRemotePolicy_CanReceive(t *testing.T) {
	for _, tt := range recvTests {
		t.Run(tt.in, func(t *testing.T) {
			rrp, err := policy.NewRegexRemotePolicy(tt.remote)
			assert.NoError(t, err)

			assert.Equal(t, tt.out, rrp.CanReceive(tt.in))
		})
	}
}

var cacheTests = []struct {
	name   string
	remote *v1.Remote
	in     []string
	out    bool
}{
	{
		"maven pom can be cached",
		mavenRemote,
		[]string{
			"/io/rest-assured/rest-assured-common/3.3.0/rest-assured-common-3.3.0.pom",
			"/com/github/djcass44/jmp-security-utils/0.1.2-beta.3/jmp-security-utils-0.1.2-beta.3.pom",
		},
		true,
	},
	{
		"maven metadata cannot be cached",
		mavenRemote,
		[]string{"/io/rest-assured/rest-assured-common/3.3.0/maven-metadata.xml"},
		false,
	},
	{
		"npm non-tarballs cannot be cached",
		npmRemote,
		[]string{"/somefile.txt", "/@prism/prism-web-0.2.1.json"},
		false,
	},
	{
		"npm tarballs can be cached",
		npmRemote,
		[]string{"/@prism/prism-web-0.2.1.tgz"},
		true,
	},
	{
		"generic is always cached",
		genericRemote,
		[]string{"/somepath.txt"},
		true,
	},
	{
		"alpine is almost always cached",
		alpineRemote,
		[]string{"/somepath.txt", "/stable-main/vim-1.2.3.tgz"},
		true,
	},
	{
		"alpine index is never cached",
		alpineRemote,
		[]string{"/latest-stable/main/APKINDEX.tar.gz"},
		false,
	},
	{
		"debian is sometimes cached",
		debianRemote,
		[]string{"/something.deb", "/something.tar.gz"},
		true,
	},
	{
		"debian files with no extension are not cached",
		debianRemote,
		[]string{"/something"},
		false,
	},
}

func TestRegexRemotePolicy_CanCache(t *testing.T) {
	for _, tt := range cacheTests {
		t.Run(tt.name, func(t *testing.T) {
			rrp, err := policy.NewRegexRemotePolicy(tt.remote)
			assert.NoError(t, err)
			for _, ii := range tt.in {
				assert.Equal(t, tt.out, rrp.CanCache(ii))
			}
		})
	}
}

func TestNewRegexRemotePolicy(t *testing.T) {
	_, err := policy.NewRegexRemotePolicy(&v1.Remote{
		AllowList: []string{"^/(github.com"},
	})
	assert.Error(t, err)

	_, err = policy.NewRegexRemotePolicy(&v1.Remote{
		BlockList: []string{"^/(github.com"},
	})
	assert.Error(t, err)
}

func TestNewRegexRemotePolicies(t *testing.T) {
	policies, err := policy.NewRegexRemotePolicies([]*v1.Remote{
		{
			AllowList: []string{"^/(github.com).*$"},
		},
		{
			AllowList: []string{"^/(gitlab.com).*$"},
		},
	})
	assert.NoError(t, err)
	assert.Len(t, policies, 2)

	policies, err = policy.NewRegexRemotePolicies([]*v1.Remote{
		{
			AllowList: []string{"^/(github.com"},
		},
		{
			AllowList: []string{"^/(gitlab.com).*$"},
		},
	})
	assert.Error(t, err)
	assert.Nil(t, policies)
}
