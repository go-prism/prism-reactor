/*
 * Copyright 2021 Django Cass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package httpclient

import (
	"crypto/tls"
	"crypto/x509"
	_ "embed"
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
	"net/url"
	"testing"
)

//go:embed testdata/tls.crt
var cert string

//go:embed testdata/tls.key
var key string

func TestBuildClient(t *testing.T) {
	var cases = []struct {
		name string
		f    func(t *testing.T)
	}{
		{"empty client is created", func(tt *testing.T) {
			// empty client is created correctly
			assert.NotNil(tt, NewClientBuilder().Build())
		}},
		{"certificates are configured correctly", func(tt *testing.T) {
			certs, _ := x509.SystemCertPool()
			length := len(certs.Subjects())
			// valid CA is appended
			client := NewClientBuilder().FromProfile(&v1.ClientProfile{
				TlsCA:   cert,
				TlsCert: cert,
				TlsKey:  key,
			}).Build()
			// confirm that the number of certs has increased by 1
			assert.Equal(t, length+1, len(client.Transport.(*http.Transport).TLSClientConfig.RootCAs.Subjects()))
			assert.Equal(t, 1, len(client.Transport.(*http.Transport).TLSClientConfig.Certificates))
		}},
		{"junk certificates are not configured", func(tt *testing.T) {
			certs, _ := x509.SystemCertPool()
			length := len(certs.Subjects())
			// valid CA is appended
			client := NewClientBuilder().FromProfile(&v1.ClientProfile{
				TlsCA:   "arstarstarst",
				TlsCert: "arstarstarstarst",
				TlsKey:  "arstarstarstarst",
			}).Build()
			// confirm that the number of certs has increased by 1
			assert.Equal(t, length, len(client.Transport.(*http.Transport).TLSClientConfig.RootCAs.Subjects()))
			assert.Equal(t, 0, len(client.Transport.(*http.Transport).TLSClientConfig.Certificates))
		}},
		{"proxy settings are configured correctly", func(tt *testing.T) {
			// custom proxy settings are configured
			client := NewClientBuilder().FromProfile(&v1.ClientProfile{
				HttpProxy:  "http://localhost:8080",
				HttpsProxy: "http://localhost:8080",
				NoProxy:    ".cluster.local",
			}).Build()
			uri, err := client.Transport.(*http.Transport).Proxy(&http.Request{
				URL: &url.URL{
					Scheme: "http",
					Host:   "localhost:8081",
				},
			})
			assert.NoError(t, err)
			// todo add an assertion here that verifies that proxy settings are in effect
			t.Log(uri)
		}},
		{"nil profile generates defaults", func(t *testing.T) {
			client := NewClientBuilder().FromProfile(nil).Build()
			assert.NotNil(t, client)
			assert.EqualValues(t, tls.VersionTLS12, client.Transport.(*http.Transport).TLSClientConfig.MinVersion)
		}},
	}

	for _, tt := range cases {
		t.Run(tt.name, tt.f)
	}
}

func TestClientBuilder_getTLSVersion(t *testing.T) {
	var cases = []struct {
		name     string
		profile  v1.ClientProfile_CipherProfile
		expected uint16
	}{
		{
			"modern returns 1.3",
			v1.ClientProfile_MODERN,
			tls.VersionTLS13,
		},
		{
			"intermediate returns 1.2",
			v1.ClientProfile_INTERMEDIATE,
			tls.VersionTLS12,
		},
		{
			"legacy returns 1.2",
			v1.ClientProfile_LEGACY,
			tls.VersionTLS12,
		},
		{
			"everything returns 1.2",
			567456,
			tls.VersionTLS12,
		},
	}

	var builder = NewClientBuilder()
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.EqualValues(t, tt.expected, builder.getTLSVersion(&tt.profile))
		})
	}
}

func TestClientBuilder_getCipherList(t *testing.T) {
	var cases = []struct {
		name        string
		profile     v1.ClientProfile_CipherProfile
		expectedLen int
	}{
		{
			"modern returns 0",
			v1.ClientProfile_MODERN,
			0,
		},
		{
			"intermediate returns 6",
			v1.ClientProfile_INTERMEDIATE,
			6,
		},
		{
			"legacy returns 18",
			v1.ClientProfile_LEGACY,
			18,
		},
		{
			"everything returns 6",
			567456,
			6,
		},
	}

	var builder = NewClientBuilder()
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			assert.EqualValues(t, tt.expectedLen, len(builder.getCipherList(&tt.profile)))
		})
	}
}
