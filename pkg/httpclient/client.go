/*
 * Copyright 2021 Django Cass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package httpclient

import (
	"crypto/tls"
	"crypto/x509"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"golang.org/x/net/http/httpproxy"
	"net/http"
	"net/url"
	"os"
)

type ClientBuilder struct {
	client *http.Client
}

// NewClientBuilder builds an *http.Client
// with a variable spec
func NewClientBuilder() *ClientBuilder {
	b := new(ClientBuilder)
	b.client = &http.Client{}

	return b
}

// Build returns the final http.Client
// that has been assembled by this ClientBuilder
func (b *ClientBuilder) Build() *http.Client {
	return b.client
}

// FromDefaults applies a sensible default configuration
// to the http.Client
func (b *ClientBuilder) FromDefaults() *ClientBuilder {
	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		},
	}
	transport := b.defaultTransport()
	transport.Proxy = http.ProxyFromEnvironment
	transport.TLSClientConfig = tlsConfig
	b.client.Transport = transport
	return b
}

// getTLSVersion returns the equivalent constant
// that describes the requested TLS profile
func (b *ClientBuilder) getTLSVersion(profile *v1.ClientProfile_CipherProfile) uint16 {
	switch *profile {
	case v1.ClientProfile_MODERN:
		return tls.VersionTLS13
	default:
		return tls.VersionTLS12
	}
}

// getCipherList returns a list of ciphers for the
// request TLS profile
//
// ciphers are generated from here - https://ssl-config.mozilla.org/
//
// the only difference is that legacy mode will not drop the tls version
// below 1.2
func (b *ClientBuilder) getCipherList(profile *v1.ClientProfile_CipherProfile) []uint16 {
	switch *profile {
	// since we set tls to 1.2, we should also use its cipher suite
	default:
		fallthrough
	case v1.ClientProfile_INTERMEDIATE:
		return []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
		}
	// tls 1.3 doesn't require cipher configuration
	case v1.ClientProfile_MODERN:
		return nil
	// ideally we should never allow this cipherset since it's garbage
	// however it needs to hang around to support legacy remotes.
	//
	// spitting out a bunch of warnings and making it difficult to select
	// via the UI should be enough for now.
	case v1.ClientProfile_LEGACY:
		log.Warning("enabling legacy TLS ciphers - HERE BE DRAGONS")
		log.Warning("YOU ARE USING BROKEN CIPHERS - you should switch your TLS profile to modern or intermediate ASAP - THIS IS NOT SECURE")
		return []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
			tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_128_CBC_SHA256,
			tls.TLS_RSA_WITH_AES_128_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_3DES_EDE_CBC_SHA,
		}
	}
}

// FromProfile applies the configuration options from a
// given v1.ClientProfile
//
// Errors loading certificates are swallowed
func (b *ClientBuilder) FromProfile(p *v1.ClientProfile) *ClientBuilder {
	if p == nil {
		log.Warning("received nil client profile, defaults values will be generated")
		return b.FromDefaults()
	}
	// try to read the system CA certs, or create a new pool
	caRoot, err := x509.SystemCertPool()
	if err != nil {
		log.WithError(err).Error("failed to read system certificate pool, creating a new one")
		caRoot = x509.NewCertPool()
	} else {
		log.Debug("successfully read x509 system certificate pool")
	}
	if p.TlsCA != "" {
		if !caRoot.AppendCertsFromPEM([]byte(p.TlsCA)) {
			log.Error("failed to append given CA cert")
		} else {
			log.Info("successfully appended CA cert to system roots")
		}
	} else {
		log.Debug("skipping CA appending as we weren't given a CA cert")
	}
	var certs []tls.Certificate
	if p.TlsCert != "" && p.TlsKey != "" {
		cert, err := tls.X509KeyPair([]byte(p.TlsCert), []byte(p.TlsKey))
		if err != nil {
			log.WithError(err).Error("failed to read given x509 keypair")
		} else {
			log.Info("successfully read x509 keypair from client profile")
			certs = append(certs, cert)
		}
	} else {
		log.Debug("skipping x509 keypair initialisation as we weren't given a keypair")
	}
	tlsConfig := &tls.Config{ //nolint:gosec
		RootCAs:      caRoot,
		Certificates: certs,
		MinVersion:   b.getTLSVersion(&p.Ciphers),
		CipherSuites: b.getCipherList(&p.Ciphers),
	}
	tlsConfig.InsecureSkipVerify = p.SkipTLSVerify
	var proxy *httpproxy.Config
	// check if our custom values are set, otherwise fetch from the environment
	if p.HttpProxy == "" {
		log.Info("importing proxy configuration from environment")
		proxy = httpproxy.FromEnvironment()
	} else {
		log.Info("using custom proxy settings")
		proxy = &httpproxy.Config{
			HTTPProxy:  p.HttpProxy,
			HTTPSProxy: p.HttpsProxy,
			NoProxy:    p.NoProxy,
			CGI:        os.Getenv("REQUEST_METHOD") != "",
		}
	}
	log.Infof("active proxy settings: %+v", proxy)
	transport := b.defaultTransport()
	transport.Proxy = func(r *http.Request) (*url.URL, error) {
		return proxy.ProxyFunc()(r.URL)
	}
	transport.TLSClientConfig = tlsConfig
	b.client.Transport = transport
	return b
}

func (b *ClientBuilder) defaultTransport() *http.Transport {
	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.MaxIdleConns = 100
	transport.MaxConnsPerHost = 100
	transport.MaxIdleConnsPerHost = 100
	transport.ForceAttemptHTTP2 = true

	return transport
}
