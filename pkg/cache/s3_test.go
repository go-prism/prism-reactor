package cache

import (
	"context"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
)

// interface guard
var _ Provider = &S3{}

func TestNewS3(t *testing.T) {
	svc := NewS3(nil)
	assert.NotNil(t, svc)
}

type mockS3 struct {
	s3iface.S3API
}

func (*mockS3) ListObjectsV2Pages(input *s3.ListObjectsV2Input, f func(output *s3.ListObjectsV2Output, b bool) bool) error {
	f(&s3.ListObjectsV2Output{
		Contents: []*s3.Object{
			{
				Key:  aws.String(filepath.Join(*input.Prefix, "foo")),
				Size: aws.Int64(10),
			},
		},
	}, true)
	return nil
}

func (*mockS3) ListObjectsV2(input *s3.ListObjectsV2Input) (*s3.ListObjectsV2Output, error) {
	return &s3.ListObjectsV2Output{
		Contents: []*s3.Object{
			{
				Key:  aws.String(filepath.Join(*input.Prefix, "foo")),
				Size: aws.Int64(10),
			},
		},
	}, nil
}

func (m *mockS3) HeadObject(req *s3.HeadObjectInput) (*s3.HeadObjectOutput, error) {
	if *req.Key == "TEST" {
		return nil, errors.New("you asked for this")
	}
	return &s3.HeadObjectOutput{}, nil
}

func (m *mockS3) GetObjectRequest(*s3.GetObjectInput) (*request.Request, *s3.GetObjectOutput) {
	return &request.Request{
		Operation:   &request.Operation{},
		HTTPRequest: httptest.NewRequest(http.MethodGet, "https://s3.amazonaws.com", nil),
	}, &s3.GetObjectOutput{}
}

func TestS3_GetSize(t *testing.T) {
	svc := &S3{
		client: &mockS3{},
		c: &S3Config{
			Bucket:    "bucket",
			Region:    "us-west-1",
			Endpoint:  "",
			PathStyle: false,
		},
	}
	size, err := svc.GetSize(context.TODO(), "test")
	assert.NoError(t, err)
	assert.Equal(t, int64(1), size.Files)
	assert.Equal(t, int64(10), size.Size)
}

func TestS3_ListObjects(t *testing.T) {
	svc := &S3{
		client: &mockS3{},
		c: &S3Config{
			Bucket:    "bucket",
			Region:    "us-west-1",
			Endpoint:  "",
			PathStyle: false,
		},
	}
	items, err := svc.ListObjects(context.TODO(), "test")
	assert.NoError(t, err)
	assert.ElementsMatch(t, items, []string{"test/foo"})
}

func TestS3_HasObject(t *testing.T) {
	svc := &S3{
		client: &mockS3{},
		c: &S3Config{
			Bucket:    "bucket",
			Region:    "us-west-1",
			Endpoint:  "",
			PathStyle: false,
		},
	}
	ok, err := svc.HasObject(context.TODO(), "test")
	assert.NoError(t, err)
	assert.True(t, ok)
}

func TestS3_HasObject_Error(t *testing.T) {
	svc := &S3{
		client: &mockS3{},
		c: &S3Config{
			Bucket:    "bucket",
			Region:    "us-west-1",
			Endpoint:  "",
			PathStyle: false,
		},
	}
	ok, err := svc.HasObject(context.TODO(), "TEST")
	assert.Error(t, err)
	assert.False(t, ok)
}

func TestS3_GetObjectURL(t *testing.T) {
	svc := &S3{
		client: &mockS3{},
		c: &S3Config{
			Bucket:    "bucket",
			Region:    "us-west-1",
			Endpoint:  "",
			PathStyle: false,
		},
	}
	_, err := svc.GetObjectURL(context.TODO(), "test")
	assert.NoError(t, err)
}
