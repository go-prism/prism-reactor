package cache

import (
	"bytes"
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/djcass44/go-tracer/tracer"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"io"
	"io/ioutil"
	"strings"
	"time"
)

type S3Config struct {
	Bucket    string
	Region    string
	Endpoint  string
	PathStyle bool
}

type S3 struct {
	c      *S3Config
	client s3iface.S3API
}

func NewS3(c *S3Config) *S3 {
	svc := new(S3)
	svc.c = c

	return svc
}

// Init creates the s3.S3 client and
// associated session.Session
func (svc *S3) Init() error {
	sess, err := svc.getSession(context.TODO())
	if err != nil {
		return err
	}
	svc.client = s3.New(sess)
	return nil
}

func (svc *S3) GetSize(ctx context.Context, path string) (*reactor.GetRefractInfoResponse, error) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id":   id,
		"path": path,
	}
	loi := &s3.ListObjectsV2Input{
		Bucket: aws.String(svc.c.Bucket),
		Prefix: aws.String(path),
	}
	log.WithFields(fields).Info("calculating data size")
	total := int64(0)
	size := int64(0)
	// loop over the paged data
	err := svc.client.ListObjectsV2Pages(loi, func(output *s3.ListObjectsV2Output, b bool) bool {
		for _, i := range output.Contents {
			total++
			size += *i.Size
		}
		return true
	})
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to iterate bucket contents")
		return nil, err
	}
	return &reactor.GetRefractInfoResponse{
		Size:     size,
		Files:    total,
		Metadata: map[string]string{},
	}, nil
}

// ListObjects fetches the names of all objects under
// a given prefix (read: folder)
func (svc *S3) ListObjects(ctx context.Context, prefix string) ([]string, error) {
	fields := log.Fields{"path": prefix}
	log.WithContext(ctx).WithFields(fields).Infof("listing objects in bucket %s", svc.c.Bucket)
	result, err := svc.client.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket: aws.String(svc.c.Bucket),
		Prefix: aws.String(prefix),
	})
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to list bucket objects")
		return nil, err
	}
	log.WithContext(ctx).WithFields(fields).Infof("successfully listed bucket with %d result(s)", len(result.Contents))
	names := make([]string, len(result.Contents))
	for i, o := range result.Contents {
		names[i] = *o.Key
	}
	return names, nil
}

// HasObject checks whether a given file exists in S3
func (svc *S3) HasObject(ctx context.Context, path string) (bool, error) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id":   id,
		"path": path,
	}
	hoi := &s3.HeadObjectInput{
		Bucket: aws.String(svc.c.Bucket),
		Key:    aws.String(path),
	}
	log.WithFields(fields).Info("checking for object existence")
	_, err := svc.client.HeadObject(hoi)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to head s3 object")
		return false, err
	}
	return true, nil
}

// GetObjectURL creates an S3 pre-signed download link
func (svc *S3) GetObjectURL(ctx context.Context, path string) (string, error) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id":   id,
		"path": path,
	}
	log.WithFields(fields).Infof("generating pre-signed url for bucket: '%s'", svc.c.Bucket)
	req, _ := svc.client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(svc.c.Bucket),
		Key:    aws.String(path),
	})
	// sign for 1 minute, which should be plenty for the client to download
	signedURL, err := req.Presign(15 * time.Minute)
	if err != nil {
		log.WithError(err).WithFields(fields).Errorf("failed to pre-sign key")
		return "", err
	}
	return signedURL, nil
}

func (svc *S3) PutObject(ctx context.Context, path string, r io.ReadCloser) error {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id":   id,
		"path": path,
	}
	sess, err := svc.getSession(ctx)
	if err != nil {
		return err
	}
	log.WithFields(fields).Info("preparing to publish object")
	// upload the file
	uploader := s3manager.NewUploader(sess)
	_, err = uploader.Upload(&s3manager.UploadInput{
		Body:   r,
		Bucket: aws.String(svc.c.Bucket),
		Key:    aws.String(path),
	})
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to upload object")
		return err
	}
	log.WithFields(fields).Info("successfully published object")
	return nil
}

// DeleteObject remotes an object from S3
func (svc *S3) DeleteObject(ctx context.Context, path string) error {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("preparing to delete object")
	// delete the file
	output, err := svc.client.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(svc.c.Bucket),
		Key:    aws.String(path),
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to delete object")
		return err
	}
	log.WithContext(ctx).WithFields(fields).Debugf("delete operation responded with: '%s'", output.String())
	return nil
}

func (svc *S3) StreamGetObject(ctx context.Context, path string) (io.ReadCloser, error) {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("preparing to stream object")
	// stream the file
	sess, err := svc.getSession(ctx)
	if err != nil {
		return nil, err
	}
	downloader := s3manager.NewDownloader(sess)

	// create a temporary buffer
	buf := aws.NewWriteAtBuffer(nil)

	n, err := downloader.Download(buf, &s3.GetObjectInput{
		Bucket: aws.String(svc.c.Bucket),
		Key:    aws.String(path),
	})
	log.WithError(err).WithContext(ctx).Debugf("downloaded %d bytes from s3", n)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to stream object")
		return nil, err
	}
	return ioutil.NopCloser(bytes.NewReader(buf.Bytes())), nil
}

// getSession gets an S3 session with the config given to the Provider constructor
func (svc *S3) getSession(ctx context.Context) (*session.Session, error) {
	id := tracer.GetContextId(ctx)
	fields := log.Fields{
		"id": id,
	}
	log.WithFields(fields).Debugf("generating s3 session with config: %+v", svc.c)
	config := &aws.Config{
		S3ForcePathStyle:              aws.Bool(svc.c.PathStyle),
		Region:                        aws.String(svc.c.Region),
		CredentialsChainVerboseErrors: aws.Bool(true),
	}
	// special config for non-aws s3
	if svc.c.Endpoint != "" {
		if !strings.HasPrefix(svc.c.Endpoint, "https") {
			config.DisableSSL = aws.Bool(true)
			log.WithFields(fields).Debugf("disabling TLS for endpoint: '%s'", svc.c.Endpoint)
		}
		config.Endpoint = aws.String(svc.c.Endpoint)
	}
	s, err := session.NewSession(config)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to create S3 session")
		return nil, err
	}
	log.WithFields(fields).Info("generated new s3 session")
	return s, nil
}
