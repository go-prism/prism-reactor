/*
 * Copyright 2021 Django Cass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package partition

import (
	"context"
	_ "embed"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGitLabMutator_getAPIAddr(t *testing.T) {
	var cases = []struct {
		in  string
		out string
		ok  bool
	}{
		{
			"https://gitlab.example.com/api/v4/projects/1/packages/maven",
			"https://gitlab.example.com/api/v4/job",
			true,
		},
		{
			"https://gitlab.example.com/api/v4/groups/1/-/packages/maven",
			"https://gitlab.example.com/api/v4/job",
			true,
		},
		{
			"https://gitlab.example.com/api/v4/packages/maven",
			"https://gitlab.example.com/api/v4/job",
			true,
		},
		{
			"https://repo1.maven.org/maven2",
			"",
			false,
		},
	}
	gm := &GitLabMutator{}
	for _, tt := range cases {
		t.Run(tt.in, func(t *testing.T) {
			out, ok := gm.getAPIAddr(tt.in)
			assert.EqualValues(t, tt.out, out)
			assert.EqualValues(t, tt.ok, ok)
		})
	}
}

//go:embed testdata/job.json
var jobJSON string

func TestGitLabMutator_Apply(t *testing.T) {
	r := mux.NewRouter()
	r.HandleFunc("/api/v4/job", func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Job-Token")
		if token == "test" {
			http.Error(w, "error", http.StatusUnauthorized)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		_, _ = w.Write([]byte(jobJSON))
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	remote := &v1.Remote{Uri: fmt.Sprintf("%s/api/v4/packages/maven", ts.URL)}
	gm := NewGitLabMutator()
	assert.NotNil(t, gm.cache)

	var cases = []struct {
		name string
		in   map[string]string
		out  string
	}{
		{
			"token is converted to user ID",
			map[string]string{
				"Job-Token": "password",
				"Accept":    "application/json",
			},
			"1",
		},
		{
			"non-normal token is converted to user ID",
			map[string]string{
				"job-TOKEN": "password",
				"Accept":    "application/json",
			},
			"1",
		},
		{
			"missing token is ignored",
			map[string]string{},
			"",
		},
		{
			"empty token is ignored",
			map[string]string{
				"Job-Token": "",
			},
			"",
		},
		{
			"api error leaves original",
			map[string]string{
				"Job-Token": "test",
			},
			"test",
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			headers := gm.Apply(context.TODO(), remote, tt.in)
			assert.EqualValues(t, tt.out, headers["Job-Token"])
		})
	}
}
