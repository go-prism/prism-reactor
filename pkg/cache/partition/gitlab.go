/*
 * Copyright 2021 Django Cass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package partition

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/djcass44/go-utils/pkg/httputils"
	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.dcas.dev/prism/prism-reactor/pkg/httpclient"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type user struct {
	ID int `json:"id"`
}

type job struct {
	User user `json:"user"`
}

// GitLabMutator helps modify the cache hash so that
// CI jobs can be partitioned correctly.
type GitLabMutator struct {
	cache map[string]string
}

// NewGitLabMutator creates a new GitLabMutator
func NewGitLabMutator() *GitLabMutator {
	return &GitLabMutator{
		cache: map[string]string{},
	}
}

// getAPIAddr returns the GitLab V4 API address
// from a given remote URL.
func (*GitLabMutator) getAPIAddr(uri string) (string, bool) {
	if !strings.Contains(uri, "/api/v4") {
		return "", false
	}
	bits := strings.SplitN(uri, "/api/v4", 2)
	if len(bits) != 2 {
		return "", false
	}
	return fmt.Sprintf("%s/api/v4/job", bits[0]), true
}

func (*GitLabMutator) withKey(vv string, headers map[string]string) map[string]string {
	h := map[string]string{}
	for k, v := range headers {
		if http.CanonicalHeaderKey(k) == JobToken {
			h[k] = vv
		} else {
			h[k] = v
		}
	}
	return h
}

// Apply attempts to fetch the user who ran the pipeline, so we can
// use that as the partition key rather than the job token.
//
// This is because the job token is different every job, so we
// never get the benefits of caching.
func (m *GitLabMutator) Apply(ctx context.Context, remote *v1.Remote, headers map[string]string) map[string]string {
	// normalise headers
	for k, v := range headers {
		headers[http.CanonicalHeaderKey(k)] = v
	}
	token, ok := headers[JobToken]
	if !ok || token == "" {
		return headers
	}
	log.WithContext(ctx).Infof("detected Job-Token in request, applying GitLab enrichment")
	metricGitLabOps.WithLabelValues(remote.GetName()).Inc()
	// check if we've got a cached value already
	if val, ok := m.cache[token]; ok && val != "" {
		log.WithContext(ctx).Debug("detected cached partition key for given ci_job_token")
		metricGitLabCache.WithLabelValues(remote.GetName()).Inc()
		return m.withKey(val, headers)
	}
	dst, ok := m.getAPIAddr(remote.GetUri())
	if !ok {
		log.WithContext(ctx).Debugf("unable to fetch gitlab API uri from remote: '%s'", remote.GetUri())
		return headers
	}
	log.WithContext(ctx).Debugf("preparing request to %s", dst)
	client := httpclient.NewClientBuilder().
		FromProfile(remote.GetClientProfile()).
		Build()
	// build the request
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, dst, nil)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to prepare request")
		return headers
	}
	// copy the headers
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	// execute the request
	start := time.Now()
	resp, err := client.Do(req)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to execute request")
		return headers
	}
	log.WithContext(ctx).Debugf("gitlab API request responded in %s with %d", time.Since(start), resp.StatusCode)
	defer resp.Body.Close()
	if !httputils.IsHTTPSuccess(resp.StatusCode) {
		log.WithContext(ctx).Warningf("gitlab API request failed with code: %d", resp.StatusCode)
		return headers
	}
	// read the body
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to read response body")
		return headers
	}
	var tokensJob job
	if err := json.Unmarshal(data, &tokensJob); err != nil {
		log.WithError(err).WithContext(ctx).Error("failed to unmarshal response")
		return headers
	}
	metricGitLabSuccess.WithLabelValues(remote.GetName()).Inc()
	log.WithContext(ctx).Infof("resolved Job-Token to user: %d", tokensJob.User.ID)
	val := strconv.Itoa(tokensJob.User.ID)
	m.cache[token] = val
	return m.withKey(val, headers)
}
