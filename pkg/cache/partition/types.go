/*
 * Copyright 2021 Django Cass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package partition

import (
	"context"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"net/http"
)

var JobToken = http.CanonicalHeaderKey("Job-Token")

type Mutator interface {
	Apply(ctx context.Context, remote *v1.Remote, headers map[string]string) map[string]string
}
