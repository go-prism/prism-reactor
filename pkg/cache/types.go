package cache

import (
	"context"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"io"
)

type Provider interface {
	GetSize(ctx context.Context, path string) (*reactor.GetRefractInfoResponse, error)
	HasObject(ctx context.Context, path string) (bool, error)
	ListObjects(ctx context.Context, prefix string) ([]string, error)
	GetObjectURL(ctx context.Context, path string) (string, error)
	StreamGetObject(ctx context.Context, path string) (io.ReadCloser, error)
	PutObject(ctx context.Context, path string, r io.ReadCloser) error
	DeleteObject(ctx context.Context, path string) error
}

type BucketInfo struct {
	Size     int64
	Files    int64
	Metadata map[string]string
}
