/*
 * Copyright 2021 Django Cass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cache

import (
	"bytes"
	"context"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"github.com/aws/aws-sdk-go/aws"
	log "github.com/sirupsen/logrus"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"io"
	"io/ioutil"
)

type S3v2 struct {
	bucket *string
	c      *s3.Client
	ps     *s3.PresignClient
}

func NewS3v2(c S3Config) (*S3v2, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.WithError(err).Error("failed to retrieve AWS config")
		return nil, err
	}
	client := s3.NewFromConfig(cfg, func(options *s3.Options) {
		options.UsePathStyle = c.PathStyle
		options.Region = c.Region
		options.EndpointResolver = s3.EndpointResolverFromURL(c.Endpoint)
	})
	return &S3v2{
		c:      client,
		ps:     s3.NewPresignClient(client),
		bucket: aws.String(c.Bucket),
	}, nil
}

func (svc *S3v2) GetSize(ctx context.Context, path string) (*reactor.GetRefractInfoResponse, error) {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("calculating path size")
	var total, size int64
	err := svc.ListObjectsV2(ctx, aws.String(path), func(t types.Object) {
		total++
		size += t.Size
	})
	if err != nil {
		return nil, err
	}
	// loop over paged data
	return &reactor.GetRefractInfoResponse{
		Size:     size,
		Files:    total,
		Metadata: map[string]string{},
	}, nil
}

func (svc *S3v2) HasObject(ctx context.Context, path string) (bool, error) {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("checking for object existence")
	_, err := svc.c.HeadObject(ctx, &s3.HeadObjectInput{
		Bucket: svc.bucket,
		Key:    aws.String(path),
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to head s3 object")
		return false, err
	}
	return true, nil
}

// ListObjects fetches the names of all objects
// under a given prefix (folder)
func (svc *S3v2) ListObjects(ctx context.Context, prefix string) ([]string, error) {
	var names []string
	err := svc.ListObjectsV2(ctx, aws.String(prefix), func(t types.Object) {
		names = append(names, *t.Key)
	})
	if err != nil {
		return nil, err
	}
	return names, nil
}

func (svc *S3v2) ListObjectsV2(ctx context.Context, prefix *string, iter func(t types.Object)) error {
	fields := log.Fields{"prefix": prefix}
	log.WithContext(ctx).WithFields(fields).Info("listing objects")
	var token *string
	for {
		log.WithContext(ctx).WithFields(fields).Debugf("fetching page with token: '%v'", token)
		result, err := svc.c.ListObjectsV2(ctx, &s3.ListObjectsV2Input{
			Bucket:            svc.bucket,
			Prefix:            prefix,
			ContinuationToken: token,
		})
		if err != nil {
			log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to list objects")
			return err
		}
		for _, o := range result.Contents {
			iter(o)
		}
		log.WithContext(ctx).WithFields(fields).Infof("fetched %d objects, truncated: %v", result.KeyCount, result.IsTruncated)
		if !result.IsTruncated {
			log.WithContext(ctx).WithFields(fields).Info("response was not truncated, exiting")
			return nil
		}
		if result.NextContinuationToken != nil {
			token = aws.String(*result.NextContinuationToken)
		}
	}
}

func (svc *S3v2) GetObjectURL(ctx context.Context, path string) (string, error) {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("generating pre-signed url")
	resp, err := svc.ps.PresignGetObject(ctx, &s3.GetObjectInput{
		Bucket: svc.bucket,
		Key:    aws.String(path),
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to generate signed url")
		return "", err
	}
	return resp.URL, nil
}

func (svc *S3v2) StreamGetObject(ctx context.Context, path string) (io.ReadCloser, error) {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("downloading object")
	downloader := manager.NewDownloader(svc.c)
	// create a temp buffer
	buf := aws.NewWriteAtBuffer(nil)
	n, err := downloader.Download(ctx, buf, &s3.GetObjectInput{
		Bucket: svc.bucket,
		Key:    aws.String(path),
	})
	log.WithContext(ctx).WithFields(fields).Debugf("downloaded %d bytes from S3", n)
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to download object")
		return nil, err
	}
	return ioutil.NopCloser(bytes.NewReader(buf.Bytes())), nil
}

func (svc *S3v2) PutObject(ctx context.Context, path string, r io.ReadCloser) error {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("uploading object")
	uploader := manager.NewUploader(svc.c)
	_, err := uploader.Upload(ctx, &s3.PutObjectInput{
		Bucket: svc.bucket,
		Key:    aws.String(path),
		Body:   r,
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to upload file")
		return err
	}
	return nil
}

func (svc *S3v2) DeleteObject(ctx context.Context, path string) error {
	fields := log.Fields{"path": path}
	log.WithContext(ctx).WithFields(fields).Info("deleting object")
	resp, err := svc.c.DeleteObject(ctx, &s3.DeleteObjectInput{
		Bucket: svc.bucket,
		Key:    aws.String(path),
	})
	if err != nil {
		log.WithError(err).WithContext(ctx).WithFields(fields).Error("failed to delete object")
		return err
	}
	log.WithContext(ctx).WithFields(fields).Infof("successfully deleted object (permanently: %v)", resp.DeleteMarker)
	return nil
}
