# Prism Reactor

The Prism Reactor is a sub-component of the Prism application.
It acts as a gateway to Remotes and Refractions and handles retrieving, storing and caching data.

## Backends

The Reactor currently supports the following backends:

* Amazon S3
* In-memory (testing only)

S3 integration works with non-Amazon S3 APIs and support explicit keys and instance profiles (e.g. `kube2iam` or `irsa`).

If you would like support added for another backend, please open an issue or MR.