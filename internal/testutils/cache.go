package testutils

import (
	"bytes"
	"context"
	"errors"
	"io"
	"io/ioutil"
	"strings"

	log "github.com/sirupsen/logrus"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc"
)

const (
	// TestPathEmpty is a file that exists but has a zero length
	TestPathEmpty = "test://empty"
	// TestPathNoOp is a file that exists and has a non-zero length
	TestPathNoOp = "test://noop"
)

type TestProvider struct {
	data map[string]string
}

func NewTestProvider() *TestProvider {
	return &TestProvider{
		data: map[string]string{},
	}
}

func (t *TestProvider) ListObjects(_ context.Context, prefix string) ([]string, error) {
	var items []string
	for k := range t.data {
		if strings.HasPrefix(k, prefix) {
			items = append(items, k)
		}
	}
	return items, nil
}

func (t *TestProvider) HasObject(_ context.Context, path string) (bool, error) {
	if strings.Contains(path, "test://") {
		log.Infof("detected test object '%s', pretending we have it!", path)
		return true, nil
	}
	_, ok := t.data[path]
	return ok, nil
}

func (*TestProvider) GetObjectURL(_ context.Context, path string) (string, error) {
	return path, nil
}

func (t *TestProvider) PutObject(_ context.Context, path string, r io.ReadCloser) error {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		log.WithError(err).Error("test provider failed to read data")
		return err
	}
	t.data[path] = string(data)
	return nil
}

func (t *TestProvider) DeleteObject(_ context.Context, path string) error {
	delete(t.data, path)
	return nil
}

func (t *TestProvider) StreamGetObject(_ context.Context, path string) (io.ReadCloser, error) {
	if strings.Contains(path, TestPathEmpty) {
		return ioutil.NopCloser(bytes.NewReader([]byte{})), nil
	}
	val, ok := t.data[path]
	if !ok {
		return nil, errors.New("404 not found")
	}
	return ioutil.NopCloser(bytes.NewReader([]byte(val))), nil
}

func (t *TestProvider) GetSize(_ context.Context, path string) (*reactor.GetRefractInfoResponse, error) {
	if strings.Contains(path, TestPathEmpty) {
		return &reactor.GetRefractInfoResponse{
			Size:     0,
			Files:    1,
			Metadata: map[string]string{},
		}, nil
	}
	var size int64
	var count int64
	for k, v := range t.data {
		if strings.HasPrefix(k, path) {
			count++
			size += int64(len(v))
		}
	}
	return &reactor.GetRefractInfoResponse{
		Size:     size,
		Files:    count,
		Metadata: map[string]string{},
	}, nil
}

type TestClient struct {
	v1.CacheClient
}

func (*TestClient) Create(context.Context, *v1.CreateEntryRequest, ...grpc.CallOption) (*v1.CreateEntryResponse, error) {
	return &v1.CreateEntryResponse{
		Entry: &v1.CacheEntry{},
	}, nil
}
func (*TestClient) Delete(context.Context, *v1.DeleteEntryRequest, ...grpc.CallOption) (*v1.DeleteEntryResponse, error) {
	return nil, nil
}
