package testutils

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/prism-rpc/service/reactor"
	"google.golang.org/grpc/metadata"
	"testing"
)

type TestReactorStream struct {
	T               *testing.T
	ExpectedCode    int32
	ExpectedContent []byte
}

func (*TestReactorStream) SetHeader(metadata.MD) error {
	panic("implement me")
}

func (*TestReactorStream) SendHeader(metadata.MD) error {
	panic("implement me")
}

func (*TestReactorStream) SetTrailer(metadata.MD) {
	panic("implement me")
}

func (*TestReactorStream) Context() context.Context {
	return context.TODO()
}

func (*TestReactorStream) SendMsg(interface{}) error {
	panic("implement me")
}

func (*TestReactorStream) RecvMsg(interface{}) error {
	panic("implement me")
}

func (s *TestReactorStream) Send(resp *reactor.GetObjectV2Response) error {
	assert.EqualValues(s.T, s.ExpectedCode, resp.GetCode())
	assert.EqualValues(s.T, s.ExpectedContent, resp.GetContent())
	return nil
}
