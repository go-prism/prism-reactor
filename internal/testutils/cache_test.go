package testutils

import "gitlab.dcas.dev/prism/prism-reactor/pkg/cache"

// interface guard
var _ cache.Provider = NewTestProvider()
