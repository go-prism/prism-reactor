package testutils

import (
	"context"
	"errors"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net/http/httptest"
)

const (
	MagicErrKey = "err"
	Magic404Key = "404"
)

type TestReactorClient struct {
	v1.ReactorClient
	Srv *httptest.Server
}

func (c *TestReactorClient) Get(_ context.Context, in *v1.GetContextRequest, _ ...grpc.CallOption) (*v1.GetContextResponse, error) {
	if in.Key == MagicErrKey || in.Key == "" {
		return nil, errors.New("you asked for this")
	} else if in.Key == Magic404Key {
		return nil, status.Error(codes.NotFound, "you asked for this")
	}
	return &v1.GetContextResponse{
		Refraction: &v1.Refraction{
			Name: in.Key,
		},
		Remotes: []*v1.Remote{
			{
				Name:          "test",
				Uri:           c.Srv.URL,
				ClientProfile: &v1.ClientProfile{},
			},
		},
	}, nil
}
