package testutils

import (
	"context"
	v1 "gitlab.com/go-prism/prism-rpc/domain/v1"
	"gitlab.com/go-prism/prism-rpc/service/api"
	"google.golang.org/grpc"
)

type TestRemotesClient struct {
	api.RemotesClient
}

func (*TestRemotesClient) List(ctx context.Context, in *api.ListRequest, opts ...grpc.CallOption) (*v1.Remotes, error) {
	return &v1.Remotes{
		Remotes: nil,
	}, nil
}
